﻿// ReSharper disable InconsistentNaming
namespace FunkaLib.Forms.Enum
{
    public enum TextBoxType
    {
        Text = 10,
        Number = 20,
        Email = 30,
        Tel = 100,
        Time = 110,
        Url = 120,
        Week = 200,
    }
}