﻿using System.Collections.Generic;
using EPiServer.Framework.Localization;
using EPiServer.Shell.ObjectEditing;

namespace FunkaLib.Forms.Enum
{
    public class EnumSelectionFactory<TEnum> : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var values = System.Enum.GetValues(typeof(TEnum));
            foreach (var value in values)
            {
                yield return new SelectItem
                {
                    Text = GetValueName(value),
                    Value = value
                };
            }
        }

        private static string GetValueName(object value)
        {
            var staticName = System.Enum.GetName(typeof(TEnum), value);

            if (staticName != null)
            {
                var localizationPath = $"/property/enum/{typeof(TEnum).Name.ToLowerInvariant()}/{staticName.ToLowerInvariant()}";

                string localizedName;
                if (LocalizationService.Current.TryGetString(localizationPath, out localizedName))
                {
                    return localizedName;
                }
            }

            return staticName;
        }
    }
}