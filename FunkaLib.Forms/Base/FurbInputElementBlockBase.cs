﻿using EPiServer.DataAnnotations;
using EPiServer.Forms.Implementation.Elements.BaseClasses;

namespace FunkaLib.Forms.Base
{
    public class FurbInputElementBlockBase : InputElementBlockBase
    {
        [Ignore]
        public override string Label { get; set; }

        [Ignore]
        public override string PredefinedValue { get; set; }

        [Ignore]
        public override string PlaceHolder { get; set; }

        [Ignore]
        public override string Description { get; set; }

        [Ignore]
        public override string EditViewFriendlyTitle { get; }

        [Ignore]
        public override int ConditionCombination { get; set; }

        [Ignore]
        public override string SatisfiedAction { get; set; }
    }
}