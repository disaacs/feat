﻿using EPiServer.Forms.Core;
using EPiServer.Forms.Core.Models;

namespace FunkaLib.Forms.Interfaces
{
    internal interface IFurbHoneyPotBlock : IElementBlock
    {
        IFormElement FormElement { get; }
    }
}