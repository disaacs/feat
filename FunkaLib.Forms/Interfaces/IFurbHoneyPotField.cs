﻿using EPiServer.Forms.Core.Models;

namespace FunkaLib.Forms.Interfaces
{
    internal interface IFurbHoneyPotField : IFormElement
    {
        string Text { get; set; }
    }
}