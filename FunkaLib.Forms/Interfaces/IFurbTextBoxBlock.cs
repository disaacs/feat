﻿using EPiServer.Forms.Core;
using EPiServer.Forms.Core.Models;
using FunkaLib.Forms.Enum;

namespace FunkaLib.Forms.Interfaces
{
    internal interface IFurbTextBoxBlock : IElementBlock
    {
        IFormElement FormElement { get; }

        AutoComplete AutoCompleteSelect { get; set; }
    }
}