﻿using EPiServer.Forms.Core.Models;

namespace FunkaLib.Forms.Interfaces
{
    public interface IFurbInformation : IFormElement
    {
        string Text { get; set; }
    }
}