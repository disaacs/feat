﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Forms.Core;
using EPiServer.Forms.Core.Events;
using EPiServer.Forms.Core.Models;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using InitializationModule = EPiServer.Web.InitializationModule;

namespace FunkaLib.Forms.Events
{
    [InitializableModule]
    [ModuleDependency(typeof(InitializationModule))]
    public class FormEventsInit : IInitializableModule
    {
        private IContentEvents contentEvents;
        private bool eventsAttached;

        public void Initialize(InitializationEngine context)
        {
            contentEvents = ServiceLocator.Current.GetInstance<IContentEvents>();

            if (!eventsAttached)
            {
                contentEvents = ServiceLocator.Current.GetInstance<IContentEvents>();
                eventsAttached = true;
            }
            FormsEvents.Instance.FormsSubmitting += Instance_FormsSubmitting;
        }

        public void Uninitialize(InitializationEngine context)
        {
            FormsEvents.Instance.FormsSubmitting -= Instance_FormsSubmitting;
            eventsAttached = false;
        }

        private void Instance_FormsSubmitting(object sender, FormsEventArgs e)
        {
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            var formRepo = ServiceLocator.Current.GetInstance<IFormRepository>();
            var content = contentLoader.Get<IContent>(e.FormsContent.ContentLink);
            var localizable = content as ILocalizable;

            if (content == null || localizable == null)
            {
                return;
            }

            var formIdentity = new FormIdentity(content.ContentGuid, localizable.Language.Name);

            var formData = (NameValueCollection)e.Data;

            var friendlyNameInfos = formRepo.GetDataFriendlyNameInfos(formIdentity);
            var pairedValues = new List<KeyValuePair<string, string[]>>();

            foreach (var key in formData.Keys)
            {
                var match = friendlyNameInfos.SingleOrDefault(x => x.ElementId.Equals(((string)key).ToLower()));

                if (match != null)
                {
                    pairedValues.Add(new KeyValuePair<string, string[]>(match.FriendlyName, formData.GetValues((string) key)));
                }
            }

            var t = pairedValues;
        }
    }
}