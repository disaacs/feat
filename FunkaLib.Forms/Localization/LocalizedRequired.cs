﻿using EPiServer.Framework.Localization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FunkaLib.Forms.Localization
{
    public class LocalizedRequiredAttribute : RequiredAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            return new[]
            {
                new ModelClientValidationRule
                {
                    ErrorMessage = LocalizationService.Current.GetString(ErrorMessage),
                    ValidationType = "required"
                }
            };
        }
    }
}