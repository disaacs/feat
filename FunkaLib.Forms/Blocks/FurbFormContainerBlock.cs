﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Implementation.Elements;
using FunkaLib.EpiCreateNewProperties.Attributes;
using System.ComponentModel.DataAnnotations;
using FunkaLib.Forms.Rendering.ContentIcon;

namespace FunkaLib.Forms.Blocks
{
    [ContentType(GUID = "f2dcf044-8c94-43ef-b61a-c84aa7af19c1", GroupName = "Form", Order = 1800)]
    [ImageUrl("~/Static/Forms/Images/formblock.png")]
    [ContentIcon(ContentIcon.FormFormContainerBlock)]
    public class FurbFormContainerBlock : FormContainerBlock
    {
        [HideOnContentCreate]
        public override ContentArea ElementsArea { get; set; }

        [HideOnContentCreate]
        public override Url RedirectToPage { get; set; }

        [ScaffoldColumn(false)]
        public override bool AllowMultipleSubmission { get; set; }

        [ScaffoldColumn(false)]
        public override bool AllowAnonymousSubmission { get; set; }

        [ScaffoldColumn(false)]
        public override bool AllowToStoreSubmissionData { get; set; }

        [Ignore]
        public override string MetadataAttribute { get; set; }

        [ScaffoldColumn(false)]
        public override string Title => ((IContent)this).Name;

        [Ignore]
        public override string ConfirmationMessage { get; set; }

        [Ignore]
        public override XhtmlString SubmitSuccessMessage { get; set; }

        [Ignore]
        public override bool ShowSummarizedData { get; set; }

        [Ignore]
        public override bool AllowExposingDataFeeds { get; set; }

        [ScaffoldColumn(false)]
        public override bool ShowNavigationBar { get; set; }

        [Ignore]
        public override string Description { get; set; }

        [Ignore]
        public override string AttributesString { get; }

        [Ignore]
        public override string EditViewFriendlyTitle { get; }

        [Ignore]
        public override bool AvailableInEditView { get; }

        [Ignore]
        public override bool IsReadOnly { get; protected set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            AllowMultipleSubmission = true;
            AllowAnonymousSubmission = true;
            AllowToStoreSubmissionData = true;
        }
    }
}