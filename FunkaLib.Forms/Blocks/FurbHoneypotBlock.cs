﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.Implementation.Elements;
using EPiServer.Forms.Implementation.Elements.BaseClasses;
using EPiServer.ServiceLocation;
using FunkaLib.Forms.Elements;
using FunkaLib.Forms.Interfaces;
using System.ComponentModel.DataAnnotations;
using FunkaLib.Forms.Rendering.ContentIcon;

namespace FunkaLib.Forms.Blocks
{
    [AvailableContentTypes(Availability.None, IncludeOn = new[] { typeof(FormContainerBlock) })]
    [ContentType(DisplayName = "Honeypot", GUID = "762B0070-E0FB-4EEC-AFCE-727ECD5E98F4", GroupName = "BasicElements", Order = 1900)]
    [ServiceConfiguration(ServiceType = typeof(IFurbHoneyPotBlock))]
    [ImageUrl("~/Static/Forms/Images/honeypot.png")]
    [ContentIcon(ContentIcon.FormCaptchaElementBlock)]
    public class FurbHoneypotBlock : InputElementBlockBase, IFurbHoneyPotBlock
    {
        private IFormElement Honeypotfield { get; set; }

        [ScaffoldColumn(false)]
        public virtual string Text { get; set; }

        public override string EditViewFriendlyTitle
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Label))
                {
                    return Label;
                }

                return !string.IsNullOrWhiteSpace(Content.Name) ? Content.Name : base.EditViewFriendlyTitle;
            }
        }

        public override IFormElement FormElement
        {
            get
            {
                Honeypotfield = new FurbHoneypotfield(Content);
                return Honeypotfield;
            }
        }
    }
}