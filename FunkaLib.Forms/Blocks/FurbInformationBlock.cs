﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Core;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.Implementation.Elements;
using FunkaLib.Forms.Elements;
using FunkaLib.Forms.Interfaces;
using System.ComponentModel.DataAnnotations;
using FunkaLib.Forms.Rendering.ContentIcon;

namespace FunkaLib.Forms.Blocks
{
    [AvailableContentTypes(Availability.None, IncludeOn = new[] { typeof(FormContainerBlock) })]
    [ContentType(GUID = "b0a3f93c-9172-4a4b-b87a-d08d3b9a7b65", GroupName = "BasicElements", Order = 1900)]
    [ImageUrl("~/Static/Forms/Images/information.png")]
    [ContentIcon(ContentIcon.ObjectUnknown)]
    public class FurbInformationBlock : ElementBlockBase
    {
        private IFurbInformation furbInformation;

        [Required]
        [CultureSpecific]
        [Display(GroupName = SystemTabNames.Content, Order = 1000)]
        public virtual XhtmlString Text { get; set; }

        [Ignore] 
        public override string Label { get; set; }

        public override IFormElement FormElement
        {
            get
            {
                if (furbInformation == null || IsModified)
                {
                    furbInformation = new FurbInformation(Content);
                }

                return furbInformation;
            }
        }

        [Ignore]
        public override string Description { get; set; }
    }
}