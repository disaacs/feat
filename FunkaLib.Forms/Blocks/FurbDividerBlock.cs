﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Core;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.Implementation.Elements;
using FunkaLib.Forms.Elements;
using FunkaLib.Forms.Interfaces;
using FunkaLib.Forms.Rendering.ContentIcon;

namespace FunkaLib.Forms.Blocks
{
    [AvailableContentTypes(Availability.None, IncludeOn = new[] { typeof(FormContainerBlock) })]
    [ContentType(GUID = "EB232D00-2F3C-4A63-999F-4712A898A825", GroupName = "BasicElements", Order = 1900)]
    [ImageUrl("~/Static/Forms/Images/divider.png")]
    [ContentIcon(ContentIcon.Minus)]
    public class FurbDividerBlock : ElementBlockBase
    {
        private IFurbDivider furbDivider;

        public override IFormElement FormElement
        {
            get
            {
                if (furbDivider == null || IsModified)
                {
                    furbDivider = new FurbDivider(Content);
                }

                return furbDivider;
            }
        }
    }
}