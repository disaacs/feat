﻿using EPiServer.Forms.EditView;
using EPiServer.ServiceLocation;

namespace FunkaLib.Forms.Configuration
{
    [ServiceConfiguration(typeof(ICustomViewLocation))]
    public class FurbFormsViewLocation : CustomViewLocationBase
    {
        public override int Order
        {
            get => 500;
            set { }
        }

        public override string[] Paths => new[] { "~/Main/Forms/Views" };
    }
}