﻿using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;
using FunkaLib.Forms.Blocks;
using System.Collections.Generic;
using System.Linq;

namespace FunkaLib.Forms.ContentValidators
{
    public class FurbSelectionBlockValidator : IValidate<FurbSelectionBlock>
    {
        public IEnumerable<ValidationError> Validate(FurbSelectionBlock instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance.Content.ContentLink.ID > 0)
            {
                if (instance.AllowMultiSelect)
                {
                    if (!instance.Items.Any())
                    {
                        validationErrors.Add(new ValidationError
                        {
                            ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/FurbSelectionBlock/Validation/ItemsCantBeEmpty"),
                            PropertyName = instance.GetPropertyName(x => x.Items),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        });
                    }
                }
                else
                {
                    if (!instance.Items.Any() || instance.Items.Count() < 2)
                    {
                        validationErrors.Add(new ValidationError
                        {
                            ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/FurbSelectionBlock/Validation/ItemsMustHaveTwoOrMore"),
                            PropertyName = instance.GetPropertyName(x => x.Items),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        });
                    }
                }
            }

            return validationErrors;
        }
    }
}