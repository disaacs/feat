﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Cms.Shell;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Shell;

namespace FunkaLib.Forms.Rendering.ContentIcon
{
    [InitializableModule]
    [ModuleDependency(typeof(InitializableModule))]
    public class ContentIconInitializer : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            var registry = context.Locate.Advanced.GetInstance<UIDescriptorRegistry>();
            var classes = GetDescriptorClasses();

            foreach (var descriptor in registry.UIDescriptors)
            {
                if (classes.ContainsKey(descriptor.ForType))
                {
                    descriptor.IconClass += " " + classes[descriptor.ForType];
                }
            }
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        private Dictionary<Type, string> GetDescriptorClasses()
        {
            var typesWithAttribute = GetTypesWithAttribute();

            var descriptors = new Dictionary<Type, string>();

            foreach (var type in typesWithAttribute)
            {
                var iconAttribute = (ContentIconAttribute) Attribute.GetCustomAttribute(type, typeof(ContentIconAttribute));

                if (iconAttribute.Icon.ToString().StartsWith("Form"))
                {
                    var iconClass = $"epi-forms-icon epi-forms-icon--small epi-forms-{iconAttribute.Icon.ToString().Remove(0, 4).ToLower()}__icon";
                    descriptors.Add(type, iconClass);
                }
                else
                {
                    var iconClass = $"epi-icon{iconAttribute.Icon}";

                    if (iconAttribute.Color != ContentIconColor.Default)
                    {
                        iconClass += $" epi-icon--{iconAttribute.Color.ToString().ToLower()}";
                    }

                    descriptors.Add(type, iconClass);
                }
            }

            return descriptors;
        }

        private static IEnumerable<Type> GetTypesWithAttribute()
        {
            var validAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic);
            var typesWithAttribute = new List<Type>();

            foreach (var assembly in validAssemblies)
            {
                try
                {
                    foreach (var item in assembly.GetTypes())
                    {
                        try
                        {
                            if (item.IsDefined(typeof(ContentIconAttribute), false))
                            {
                                typesWithAttribute.Add(item);
                            }
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                }
                catch
                {
                    // ignored
                }
            }

            return typesWithAttribute;
        }
    }
}