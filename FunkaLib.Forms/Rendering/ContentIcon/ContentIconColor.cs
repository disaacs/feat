﻿namespace FunkaLib.Forms.Rendering.ContentIcon
{
    public enum ContentIconColor
    {
        Default,
        Inverted,
        Active,
        Success,
        Danger,
        Warning,
        Info
    }
}
