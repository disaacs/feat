﻿using EPiServer.Forms.EditView;
using EPiServer.Shell;
using FunkaLib.Forms.Blocks;

namespace FunkaLib.Forms.Descriptors
{
    [UIDescriptorRegistration]
    public class FurbFormContainerBlockDescriptor : FormElementBlockDescriptor<FurbFormContainerBlock>
    {
    }
}