﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using FunkaLib.Forms.Blocks;
using FunkaLib.Forms.Interfaces;
using System;

namespace FunkaLib.Forms.Elements
{
    public class FurbTextBox : FormElement, IFurbTextBox
    {
        public FurbTextBox(IContent sourceContent) : base(sourceContent)
        {
        }

        private FurbTextBoxBlock FurbTextBoxBlock
        {
            get => SourceContent as FurbTextBoxBlock;
            set => throw new NotSupportedException();
        }

        public string Text
        {
            get => FurbTextBoxBlock != null ? FurbTextBoxBlock.Text : string.Empty;
            set => throw new NotImplementedException();
        }

        public string LabelText
        {
            get => FurbTextBoxBlock != null ? FurbTextBoxBlock.Label : string.Empty;

            set => throw new NotImplementedException();
        }
    }
}