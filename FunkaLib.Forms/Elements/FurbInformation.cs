﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using FunkaLib.Forms.Blocks;
using FunkaLib.Forms.Interfaces;
using System;

namespace FunkaLib.Forms.Elements
{
    public class FurbInformation : FormElement, IFurbInformation
    {
        public FurbInformation(IContent sourceContent) : base(sourceContent)
        {
        }

        private FurbInformationBlock FurbInformationBlock
        {
            get => SourceContent as FurbInformationBlock;
            set => throw new NotSupportedException();
        }

        public string Text { get; set; }
    }
}