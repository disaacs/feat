﻿using EPiServer.Web.Mvc;
using FunkaLib.Forms.Blocks;
using System.Web.Mvc;

namespace FunkaLib.Forms.Controllers
{
    public class FurbTextBoxBlockController : BlockController<FurbTextBoxBlock>
    {
        public override ActionResult Index(FurbTextBoxBlock currentBlock)
        {
            return PartialView("FurbTextBoxBlock", currentBlock);
        }
    }
}