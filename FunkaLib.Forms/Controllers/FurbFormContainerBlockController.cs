﻿using EPiServer.Forms.Controllers;
using EPiServer.Forms.Implementation.Elements;
using System.Web.Mvc;

namespace FunkaLib.Forms.Controllers
{
    public class FurbFormContainerBlockController : FormContainerBlockController
    {
        [AllowAnonymous]
        public override ActionResult Index(FormContainerBlock currentBlock)
        {
            if (currentBlock.RedirectToPage != null)
            {
                return base.Index(currentBlock);
            }

            return new ContentResult
            {
                Content = "<p class='validation-summary-errors'>Det här formuläret saknar tacksida och kan därför inte visas!</p>" +
                          "<p class='validation-summary-errors'>This form has no thank you page and can therefore not display!</p>"
            };
        }
    }
}