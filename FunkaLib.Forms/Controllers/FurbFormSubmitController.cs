﻿using EPiServer.Forms.Controllers;
using EPiServer.Logging;
using EPiServer.ServiceLocation;
using System;
using System.Web.Mvc;

namespace FunkaLib.Forms.Controllers
{
    [ServiceConfiguration(typeof(IDataSubmitController))]
    public class FurbFormSubmitController : DataSubmitController
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(FurbFormSubmitController));

        [HttpPost]
        public override ActionResult Submit()
        {
            return base.Submit();
        }

        [AllowAnonymous]
        public override ActionResult GetFormInitScript(Guid formGuid, string formLanguage)
        {
            return base.GetFormInitScript(formGuid, formLanguage);
        }
    }
}