﻿using EPiServer.Web.Mvc;
using FunkaLib.Forms.Blocks;
using System.Web.Mvc;

namespace FunkaLib.Forms.Controllers
{
    public class FurbDropDownBlockController : BlockController<FurbDropDownBlock>
    {
        public override ActionResult Index(FurbDropDownBlock currentBlock)
        {
            return PartialView("FurbDropDown", currentBlock);
        }
    }
}