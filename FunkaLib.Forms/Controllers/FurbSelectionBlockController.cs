﻿using EPiServer.Web.Mvc;
using FunkaLib.Forms.Blocks;
using System.Web.Mvc;

namespace FunkaLib.Forms.Controllers
{
    public class FurbSelectionBlockController : BlockController<FurbSelectionBlock>
    {
        public override ActionResult Index(FurbSelectionBlock currentBlock)
        {
            return PartialView(GetViewName(currentBlock.AllowMultiSelect), currentBlock);
        }

        private string GetViewName(bool allowMultiSelect)
        {
            return allowMultiSelect ? "FurbCheckList" : "FurbRadioList";
        }
    }
}