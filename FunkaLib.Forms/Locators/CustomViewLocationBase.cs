﻿using EPiServer.Forms.EditView;
using EPiServer.Forms.Helpers.Internal;

namespace FunkaLib.Forms.Locators
{
    public abstract class CustomViewLocationBase : ICustomViewLocation
    {
        public virtual int Order { get; set; }

        public abstract string[] Paths { get; }

        public virtual string GetDefaultViewLocation()
        {
            return "~" + ModuleHelper.ToResource(GetType(), "Views/ElementBlocks");
        }
    }
}