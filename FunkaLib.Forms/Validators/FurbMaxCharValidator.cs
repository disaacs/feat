﻿using EPiServer.Forms.Core.Models;
using EPiServer.Forms.Core.Validation;
using EPiServer.Forms.EditView;
using EPiServer.Framework.Localization;
using System;
using System.Collections.Generic;

namespace FunkaLib.Forms.Validators
{
    public class FurbMaxCharValidator : ElementValidatorBase, IPreviewableTextBox
    {
        public override int ValidationOrder => 150;

        private int maxCharCount;

        public int MaxCharCount => maxCharCount;

        public override bool? Validate(IElementValidatable targetElement)
        {
            try
            {
                _validationService.Service.GetValidatorMessage(GetType(), "a");
                var submittedValue = targetElement.GetSubmittedValue();
                var value = (string)submittedValue;

                if (string.IsNullOrWhiteSpace(value))
                {
                    return true;
                }

                var charArray = value.ToCharArray();

                return MaxCharCount <= 0 || charArray.Length <= MaxCharCount;
            }
            catch (InvalidCastException)
            {
                return false;
            }
        }

        public override void Initialize(string rawStringOfSettings)
        {
            base.Initialize(rawStringOfSettings);

            maxCharCount = 0;
            int.TryParse(rawStringOfSettings, out maxCharCount);
        }

        public override IValidationModel BuildValidationModel(IElementValidatable targetElement)
        {
            if (Model == null)
            {
                _model = new ValidationModelBase();
            }

            if (MaxCharCount <= 0)
            {
                return _model;
            }

            _model.Message = _validationService.Service.GetValidatorMessage(GetType());
            _model.AdditionalAttributes = new Dictionary<string, string> { { "number", MaxCharCount.ToString() }, { "chars", LocalizationService.Current.GetString("/episerver/forms/validators/FunkaLib.Forms.Validators.FurbMaxCharValidator/chars") } };

            return _model;
        }
    }
}