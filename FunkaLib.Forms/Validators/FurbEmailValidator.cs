﻿using EPiServer.Forms.Core.Models;
using EPiServer.Forms.Core.Validation.Internal;

namespace FunkaLib.Forms.Validators
{
    public class FurbEmailValidator : RegularExpressionValidatorBase
    {
        public override int ValidationOrder => 120;

        public override IValidationModel Model
        {
            get
            {
                if (_model != null)
                {
                    return _model;
                }

                const string str = "[a-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
                _model = GetRegularExpressionValidationModel(str, str);

                return _model;
            }
        }
    }
}