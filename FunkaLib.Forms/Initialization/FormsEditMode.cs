﻿using Elmah;
using EPiServer.Data.Dynamic;
using EPiServer.DataAbstraction;
using EPiServer.Forms.Implementation.Elements;
using EPiServer.ServiceLocation;
using FunkaLib.Forms.Blocks;
using System;
using System.Linq;

namespace FunkaLib.Forms.Initialization
{
    public class FormsEditModeCleaner
    {
        private readonly TabDefinition conditionsTab;

        private readonly TabDefinition contentTab;
        private readonly IContentTypeRepository contentTypeRepository;
        private readonly IPropertyDefinitionRepository propertyDefinitionRepository;
        private readonly TabDefinition settingsTab;

        public FormsEditModeCleaner(IContentTypeRepository contentTypeRepository, IPropertyDefinitionRepository propertyDefinitionRepository)
        {
            this.contentTypeRepository = contentTypeRepository;
            this.propertyDefinitionRepository = propertyDefinitionRepository;
        }

        public FormsEditModeCleaner()
        {
            contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            propertyDefinitionRepository = ServiceLocator.Current.GetInstance<IPropertyDefinitionRepository>();
            var tabDefinitionRepository = ServiceLocator.Current.GetInstance<ITabDefinitionRepository>();

            var allTabs = tabDefinitionRepository.List().ToList();

            contentTab = allTabs.FirstOrDefault(x => x.Name.Equals("Content"));
            conditionsTab = allTabs.FirstOrDefault(x => x.Name.Equals("Conditions"));
            settingsTab = allTabs.FirstOrDefault(x => x.Name.Equals("PageSettings"));
        }

        public void Clean()
        {
            try
            {
                HidedefaultTypes();

                CleanFormBlock();
                CleanTextElement();
                CleanSelectionElement();
                CleanDropDownElement();
                CleanInformationElement();
                CleanDivider();
                CleanHoneypot();
                CleanSubmit();
                CleanUpload();
                CleanVisitorDataHidden();
                CleanPredefinedHidden();
            }
            catch (Exception exception)
            {
                try
                {
                    ErrorSignal.FromCurrentContext().Raise(exception);
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch
                {
                }
            }
        }

        private void HidedefaultTypes()
        {
            var types = new[]
            {
                typeof(TextboxElementBlock),
                typeof(TextareaElementBlock),
                typeof(CaptchaElementBlock),
                typeof(ImageChoiceElementBlock),
                typeof(ParagraphTextElementBlock),
                typeof(FormStepBlock),
                typeof(RangeElementBlock),
                typeof(UrlElementBlock),
                typeof(ResetButtonElementBlock),
                typeof(NumberElementBlock),
                typeof(SelectionElementBlock),
                typeof(FormContainerBlock),
                typeof(ChoiceElementBlock)
            };

            foreach (var type in types)
            {
                try
                {
                    var currentType = contentTypeRepository.Load(type);

                    if (currentType != null)
                    {
                        currentType = (ContentType)currentType.CreateWritableClone();
                        currentType.IsAvailable = false;
                        contentTypeRepository.Save(currentType);
                    }
                }
                catch
                {
                    // ignored
                }
            }

            try
            {
                var uploadType = contentTypeRepository.Load(typeof(FileUploadElementBlock));

                if (uploadType != null && uploadType.IsAvailable == false)
                {
                    var clone = (ContentType)uploadType.CreateWritableClone();

                    if (clone != null)
                    {
                        clone.IsAvailable = true;
                        contentTypeRepository.Save(clone);
                    }
                }

                var predefinedHiddenType = contentTypeRepository.Load(typeof(PredefinedHiddenElementBlock));

                if (predefinedHiddenType != null && predefinedHiddenType.IsAvailable == false)
                {
                    var clone = (ContentType)predefinedHiddenType.CreateWritableClone();

                    if (clone != null)
                    {
                        clone.IsAvailable = true;
                        contentTypeRepository.Save(clone);
                    }
                }

                var visitorDataHiddenType = contentTypeRepository.Load(typeof(VisitorDataHiddenElementBlock));

                if (visitorDataHiddenType != null && visitorDataHiddenType.IsAvailable == false)
                {
                    var clone = (ContentType)visitorDataHiddenType.CreateWritableClone();

                    if (clone != null)
                    {
                        clone.IsAvailable = true;
                        contentTypeRepository.Save(clone);
                    }
                }
            }
            catch
            {
                // ignored
            }
        }

        private void CleanFormBlock()
        {
            var furbformcontainerblock = contentTypeRepository.Load<FurbFormContainerBlock>();

            if (furbformcontainerblock == null)
            {
                return;
            }

            var furbpropertieskvp = (PropertyDefinitionCollection)furbformcontainerblock.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in furbpropertieskvp)
            {
                var clone = property.CreateWritableClone();

                if (property.Name.Equals("SendEmailAfterSubmissionActor"))
                {
                    clone.FieldOrder = 450;
                    clone.Tab = settingsTab;
                }

                if (property.Name.Equals("CallWebhookAfterSubmissionActor"))
                {
                    clone.FieldOrder = 500;
                    clone.Tab = settingsTab;
                }

                if (property.Name.Equals("ElementsArea"))
                {
                    clone.FieldOrder = 1;
                    clone.Tab = contentTab;
                }

                if (property.Name.Equals("RedirectToPage"))
                {
                    clone.FieldOrder = 1000;
                    clone.Tab = contentTab;
                }

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanTextElement()
        {
            var furbtextboxblock = contentTypeRepository.Load<FurbTextBoxBlock>();

            if (furbtextboxblock == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)furbtextboxblock.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();

                clone.Tab = contentTab;

                HandleConditions(clone);

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanSelectionElement()
        {
            var furbselectionblock = contentTypeRepository.Load<FurbSelectionBlock>();

            if (furbselectionblock == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)furbselectionblock.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();
                clone.Tab = contentTab;

                if (property.Name.Equals("Label") || property.Name.Equals("Description") || property.Name.Equals("PlaceHolder") || property.Name.Equals("Feed"))
                {
                    clone.DisplayEditUI = false;
                }
                else if (property.Name.Equals("Validators"))
                {
                    clone.FieldOrder = 4;
                }
                else if (property.Name.Equals("AllowMultiSelect"))
                {
                    clone.FieldOrder = 3;
                }
                else if (property.Name.Equals("Items"))
                {
                    clone.FieldOrder = 1;
                    clone.Required = false;
                    clone.DisplayEditUI = true;
                }
                else if (property.Name.Equals("ValidationMessage"))
                {
                    clone.FieldOrder = 5;
                }
                else if (property.Name.Equals("OptionList"))
                {
                    clone.DisplayEditUI = false;
                    clone.EditCaption = "";
                }

                HandleConditions(clone);

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanDropDownElement()
        {
            var furbdropdownblock = contentTypeRepository.Load<FurbDropDownBlock>();

            if (furbdropdownblock == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)furbdropdownblock.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();

                if (property.Name.Equals("Label") || property.Name.Equals("Description") || property.Name.Equals("PlaceHolder") || property.Name.Equals("Feed"))
                {
                    clone.DisplayEditUI = false;
                }
                else if (property.Name.Equals("Validators"))
                {
                    clone.FieldOrder = 4;
                }
                else if (property.Name.Equals("AllowMultiSelect"))
                {
                    clone.DisplayEditUI = false;
                    clone.FieldOrder = 3;
                }
                else if (property.Name.Equals("Items"))
                {
                    clone.FieldOrder = 1;
                    clone.Required = false;
                    clone.DisplayEditUI = true;
                }
                else if (property.Name.Equals("OptionList"))
                {
                    clone.DisplayEditUI = false;
                    clone.EditCaption = "";
                }

                clone.Tab = contentTab;

                HandleConditions(clone);

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanHoneypot()
        {
            var dividerBlock = contentTypeRepository.Load<FurbHoneypotBlock>();

            if (dividerBlock == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)dividerBlock.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();
                clone.DisplayEditUI = false;
                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanInformationElement()
        {
            var contentType = contentTypeRepository.Load<FurbInformationBlock>();

            if (contentType == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)contentType.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();
                clone.Tab = settingsTab;

                if (property.Name.Equals("Label") || property.Name.Equals("Description") || property.Name.Equals("PlaceHolder") || property.Name.Equals("Feed"))
                {
                    clone.DisplayEditUI = false;
                }
                else if (property.Name.Equals("Text"))
                {
                    clone.Tab = contentTab;
                    clone.FieldOrder = 1;
                }

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanDivider()
        {
            var contentType = contentTypeRepository.Load<FurbDividerBlock>();

            if (contentType == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)contentType.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();

                clone.DisplayEditUI = false;
                clone.Tab = settingsTab;
                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanVisitorDataHidden()
        {
            var contentType = contentTypeRepository.Load<VisitorDataHiddenElementBlock>();

            if (contentType == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)contentType.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();

                if (property.Name.Equals("VisitorDataSources"))
                {
                    clone.Tab = contentTab;
                    clone.DisplayEditUI = true;
                }
                else
                {
                    clone.DisplayEditUI = false;
                    clone.Tab = settingsTab;
                }

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanPredefinedHidden()
        {
            var contentType = contentTypeRepository.Load<PredefinedHiddenElementBlock>();

            if (contentType == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)contentType.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();

                if (property.Name.Equals("PredefinedValue"))
                {
                    clone.Tab = contentTab;
                    clone.DisplayEditUI = true;
                }
                else
                {
                    clone.DisplayEditUI = false;
                    clone.Tab = settingsTab;
                }

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanUpload()
        {
            var furbselectionblock = contentTypeRepository.Load<FileUploadElementBlock>();

            if (furbselectionblock == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)furbselectionblock.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();
                clone.Tab = contentTab;

                if (property.Name.Equals("Description"))
                {
                    clone.DisplayEditUI = false;
                }
                else if (property.Name.Equals("AllowMultiple"))
                {
                    clone.FieldOrder = 1;
                }
                else if (property.Name.Equals("Validators"))
                {
                    clone.FieldOrder = 2;
                }
                else if (property.Name.Equals("FileTypes"))
                {
                    clone.FieldOrder = 4;
                }
                else if (property.Name.Equals("FileSize"))
                {
                    clone.FieldOrder = 3;
                }

                HandleConditions(clone);

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void CleanSubmit()
        {
            var submitbtnblock = contentTypeRepository.Load<SubmitButtonElementBlock>();

            if (submitbtnblock == null)
            {
                return;
            }

            var propertieskvp = (PropertyDefinitionCollection)submitbtnblock.ToPropertyBag().FirstOrDefault(x => x.Value is PropertyDefinitionCollection).Value;

            foreach (var property in propertieskvp)
            {
                var clone = property.CreateWritableClone();
                clone.Tab = contentTab;

                if (property.Name.Equals("FinalizeForm") || property.Name.Equals("Image") || property.Name.Equals("RedirectToPage") || property.Name.Equals("Description"))
                {
                    clone.DisplayEditUI = false;
                    clone.Tab = settingsTab;
                }

                if (property.Name.Equals("Label"))
                {
                    clone.DisplayEditUI = true;
                    clone.Tab = contentTab;
                }

                HandleConditions(clone);

                propertyDefinitionRepository.Save(clone);
            }
        }

        private void HandleConditions(PropertyDefinition clone)
        {
            if (clone.Name.Equals("SatisfiedAction"))
            {
                clone.FieldOrder = 10;
                clone.DisplayEditUI = false;
                clone.Tab = conditionsTab;
            }
            else if (clone.Name.Equals("ConditionCombination"))
            {
                clone.FieldOrder = 11;
                clone.DisplayEditUI = false;
                clone.Tab = conditionsTab;
            }
            else if (clone.Name.Equals("Conditions"))
            {
                clone.FieldOrder = 12;
                clone.DisplayEditUI = false;
                clone.Tab = conditionsTab;
            }
        }
    }
}