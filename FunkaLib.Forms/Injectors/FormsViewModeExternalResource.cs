﻿using EPiServer.Forms.Implementation;
using EPiServer.ServiceLocation;
using System;
using System.Collections.Generic;

namespace FunkaLib.Forms.Injectors
{
    [ServiceConfiguration(ServiceType = typeof(IViewModeExternalResources))]
    public class FormsViewModeExternalResource : IViewModeExternalResources
    {
        public IEnumerable<Tuple<string, string>> Resources
        {
            get
            {
                var arrRes = new List<Tuple<string, string>>
                {
                    new Tuple<string, string>("script", "/Static/Forms/Js/EPiFormsCustom.js"),
                };

                return arrRes;
            }
        }
    }
}
