﻿using EPiServer.Core;
using EPiServer.ServiceLocation;
using FunkaLib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver;
using FunkaLib.Forms.Enum;
using FunkaLib.Forms.Interfaces;
using System.Collections.Generic;

namespace FunkaLib.Forms.VisibilityResolver
{
    [ServiceConfiguration(typeof(ILayoutVisibilityResolver))]
    public class AutoCompleteVisibilityResolver : ILayoutVisibilityResolver
    {
        public IEnumerable<string> GetHiddenTabs(IContent content)
        {
            return new List<string>();
        }

        public IEnumerable<string> GetHiddenProperties(IContent content)
        {
            var hiddenProperties = new List<string>();

            if (!(content is IFurbTextBoxBlock furbTextBoxBlock))
            {
                return hiddenProperties;
            }

            if (furbTextBoxBlock.AutoCompleteSelect != AutoComplete.Other)
            {
                hiddenProperties.Add("AutoComplete");
            }

            return hiddenProperties;
        }
    }
}