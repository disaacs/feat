Import-Module WebAdministration

$hostName = "feat.local"
$directoryPath = "C:\EpiServer\Sites\feat\wwwroot"
$hostsFilename = "C:\Windows\System32\drivers\etc\hosts"
$appPoolDotNetVersion = "v4.0"
$licenseFile = "C:\EpiServer\License\License.config"

#Navigate to the app pools root
cd IIS:\AppPools\

#Check if the app pool exists if not, creats it
if (!(Test-Path $hostName -pathType container))
{
	Write-Host "Creating app pool" $hostName
    $appPool = New-Item $hostName
    $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $appPoolDotNetVersion
    $appPool | set-item 
}

#Navigate to the sites root
cd IIS:\Sites\

#Check if the site exists
if (Test-Path $hostName -pathType container)
{
	Read-Host -Prompt "Both app pool and site exists. No work is done! Press Enter to exit"
	
    return
}

Write-Host "Creating the site" $hostName
$iisApp = New-Item $hostName -bindings @{protocol="http";bindingInformation=":80:" + $hostName} -physicalPath $directoryPath
$iisApp | Set-ItemProperty -Name "applicationPool" -Value $hostName

Write-Host "Add to host file 127.0.0.1 for " $hostName
"`n127.0.0.1 `t" + $hostName | Out-File -encoding ASCII -append $hostsFilename

Write-Host "Copying licenseFile to wwwroot" $hostName
Copy-Item $licenseFile "$directoryPath\License.config"

Read-Host -Prompt "Press Enter to exit"