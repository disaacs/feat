//Requires *jQuery Validation Plugin v1.14.0 and above(jQuery.Validate)
if (!jQuery().validate) {
    console.error("this library requires jQuery.Validate please ensure it's installed correctly.");
} else {
    $("form:not(#jsFunkaLoginForm):not(.EPiServerForms)").each(function () {
        var $form = $(this);
        var errorListContainerActivated = false;

        $form.validate({
            ignore: ":hidden"
            , focusInvalid: false
            , wrapper: "div"
            , errorElement: "span"
            , onfocusout: function (element, event) {
                var moreThan2Elements =
                    $form.find(("input[type='text'], input[type='email'], input[type='tel'], input[type='number'], input[type='range'], input[type='password'], textarea").length > 2);
                if (moreThan2Elements) {
                    var validField = $(element).valid();
                    if (validField) {
                        $(element).parents(".form-field:first").removeClass("errorOnThis");
                        $(element).parents(".form-field").find("label:first").find("span.customValidationMessage").remove();
                        $(element).parents(".form-field").find("label:first").find("br").remove();
                    }
                    else {
                        //Hack
                        if (element.type !== "file") {
                            $(element).parents(".form-field:first").addClass("errorOnThis");
                        }
                        else {
                            if (element.type === "file" && !$(element).attr("aria-invalid")) {
                                $(element).parents(".form-field:first").removeClass("errorOnThis");
                                var $label = $(element).parents(".form-field:first").find("label:first");
                                $label.find(".customValidationMessage").remove();
                                $label.find("br").remove();
                            }
                        }
                    }
                    //var validator = $form.validate();
                    //InvalidHandlerFunction(event, validator, errorListContainerActivated);
                    $("form:not(#jsFunkaLoginForm)").find("input[type='submit']").on("click", function () {
                        $form.valid();
                    });
                }
            }
            , invalidHandler: function (event, validator) {
                errorListContainerActivated = true;
                InvalidHandlerFunction(event, validator, errorListContainerActivated);
            }
            , errorPlacement: function (error, element) {
                console.log(error + " " + element);
                return false;
            }
            , showErrors: function (errorMap, errorList) {
                if (!$.isEmptyObject(errorMap)) {
                    $($form).find(".form-field");

                    var message;
                    var element;
                    for (var i = 0; i < errorList.length; i++) {
                        message = errorList[i].message;
                        element = errorList[i].element;
                        $(element).parents(".form-field:first").addClass("errorOnThis");
                        var span = $(element).parents(".form-field").find("label:first")
                            .find("span.customValidationMessage");
                        if (span.length) {
                            span.replaceWith("<span class='customValidationMessage'>" + message + "</span>")
                        } else {
                            if ($(element).attr("type") === "radio") {
                                $(element).parents(".form-field").find("legend:first").append(
                                    "<br><label><span class='customValidationMessage'>" + message + "</span></label>");
                            }
                            else {
                                $(element).parents(".form-field").find("label:first")
                                    .append("<br><span class='customValidationMessage'>" + message + "</span>");
                            }
                        }
                    }
                }
            }
        });

        //Hack to replace standard message from jQuery
        $.validator.messages.email = $form.find("input[type='email']:first").data("val-regex");

        //RequiredValidator
        $form.find("input[required], textarea[required]").each(function () {
            var requiredMessage = $(this).data("val-required");
            var $self = $(this);
            $self.rules('add', {
                messages: {
                    required: requiredMessage
                }
            });
        });

        //RegExValidator
        $.validator.addMethod('regEx', function (value, element, param) {
            var regExPattern = new RegExp($(element).data("val-regex-pattern"));
            return regExPattern.test(value);
        }, $.validator.messages.regEx);

        $form.find("[data-val-regex!=''][data-val-regex]").each(function () {
            var $self = $(this);
            $self.rules('add', {
                regEx: true,
                messages: {
                    regEx: $(this).data("val-regex"),
                }
            });
        });

        //StringLengthValidator
        $.validator.addMethod('stringLength', function (value, element, param) {
            var minLenght = $(element).data("val-length-min");
            var maxLenght = $(element).data("val-length-max");

            return $(element).val().length >= minLenght && $(element).val().length <= maxLenght;
        }, $.validator.messages.stringLength);

        $form.find("[data-val-length!=''][data-val-length]").each(function () {
            var $self = $(this);
            $self.rules('add', {
                stringLength: true,
                messages: {
                    stringLength: $(this).data("val-length"),
                }
            });
        });

        //CompareValidator
        $.validator.addMethod('compare', function (value, element, param) {
            otherName = $(element).data("val-equalto-other").replace("*.", "");
            var compareTo = $("input[name*=" + otherName + "]");

            return compareTo.val() === $(element).val();
        }, $.validator.messages.compare);

        $form.find("[data-val-equalto!=''][data-val-equalto]").each(function () {
            var $self = $(this);
            $self.rules('add', {
                compare: true,
                messages: {
                    compare: $(this).data("val-equalto"),
                }
            });
        });
    });

    generateMarkUp = function (form, numberOfInvalids, errors) {
        var listItems = [];
        for (var i = 0; i < errors.length; i++) {
            listItems.push("<li>" + errors[i] + "</li>")
        }
        var translatedValue = $(form).data("erroramount");
        var headerMessage;
        if (translatedValue !== "" && translatedValue !== undefined && translatedValue.indexOf("{amount}") > -1) {
            var splittedStr = translatedValue.replace("{amount}", numberOfInvalids);
            headerMessage = splittedStr;
        } else {
            headerMessage = "[Form has missing data {amount} attribute for error amount!]";
        }

        return "<div class='errorLabelContainer'>" +
            "<div class='validation-summary-errors'>" +
            "<h2 class='errorContainerHeader'>" + headerMessage + "</h2>" +
            "<ul class='errorList'> " + listItems.join(' ')
            + "</ul>" +
            "</div>" +
            "</div>";
    }

    function InvalidHandlerFunction(event, validator, errorListContainerActivated) {
        console.log("buttonPressed: " + errorListContainerActivated);
        var $form = $(this);
        var amountOfErrors = validator.numberOfInvalids();
        var form = event.currentTarget;
        var errors = validator.errorList;
        var errorMessages = [];

        for (var i = 0; i < errors.length; i++) {
            var invalidField = errors[i].element;
            errorMessages.push($(invalidField).data("label"));
        }

        //Only show summary container for forms with more than 1 input field
        if ($(form).find("input[type='text'], input[type='email'], input[type='tel'], input[type='number'], input[type='range'], input[type='password'], textarea").length > 1) {
            var markUp = generateMarkUp(form, amountOfErrors, errorMessages);
            var errorLabelContainer = $(form).find(".errorLabelContainer");
            if (!errorLabelContainer.length && errorListContainerActivated == true) {
                $(form).prepend(markUp)
            }
            else {
                $(errorLabelContainer).html(markUp);
            }

            var id = $form.attr("id");
            if (id === "jsFileUploadForm") {
                var newContainer = $(form).find(".errorLabelContainer");
                if (newContainer.length) {
                    $('html,body').animate({ scrollTop: $(newContainer).offset().top }, 'slow');
                    var header = $(newContainer).find(".errorContainerHeader");
                    if (header.length) {
                        $(header).attr("tabindex", -1);
                        $(header).focus();
                    }
                }
            }
            if (amountOfErrors < 1) { $(".validation-summary-errors").hide(); }
        }
    }
}