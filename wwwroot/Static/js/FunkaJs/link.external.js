﻿$(document).ready(function() {
    $('main a[href]').filter(function() {
        return this.hostname && this.hostname !== location.hostname;
    }).addClass("external-link").attr("target", "_blank");
});