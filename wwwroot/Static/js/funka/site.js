﻿var F = F || {};

F.Cookies = {
    exists: function (name) {
        var cookiee = Cookies.get(name);
        var cookieIsSet = typeof cookiee === 'undefined';

        return cookieIsSet;
    }
}

$(document).ready(function () {
    if (F.Cookies.exists("importantnoticeinfocontaineronehour")) {
        $("body").find(".js-importantnotice-info-container").show();
    }

    var shareLinksToggle = new funkanu.ariatoggle({
        container: "body",
        triggerSelector: "#sharemail",
        target: function ($elem) {
            return $('#emailtip');
        },
        toggleAction: function ($target) {
            $target.slideToggle("300", function () {
                if ($target.is(':visible')) {
                    $target.find('input').first().focus();
                }

            });
        }
    });

    var dt = new Date();
    var date = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();

    $("input[data-label='ThisIsAFieldThatShouldMakeTheBootToFail']").val(date);

    $('.teaserText').matchHeight();
});

if (F.Cookies.exists("importantnoticeinfocontaineronehour")) {
    $("body").on("click",
        ".js-close-importantnotice-info",
        function(e) {
            e.preventDefault();
            var $target = $(this).closest(".js-importantnotice-info-container");
            $target.slideUp(400,
                function() {
                    $(this).remove();
                });
            var in60Minutes = 1 / 24;
            Cookies.set('importantnoticeinfocontaineronehour', 1, { expires: in60Minutes });
        });
}