﻿(function ($, window, document) {
    var baseApiUrl = '/api/MobileMenu';
    var mobileMenuJson = null;
    var currentId;
    var nextLevelText;

    // DOM ready
    $(function () {
        var $mobileMenu = $('#MobileMenu');
        currentId = $mobileMenu.data('currentid');
        nextLevelText = $mobileMenu.data('nextlevel');

        var hiddenChildren = $('.childrenListContainer').filter(function (item) {
            var ariaExpandedValue = $(item).closest('li').find('.haveLinks').first().attr('aria-expanded');
            return ariaExpandedValue != null && ariaExpandedValue.toLower() === "false";
        }).hide();

        setupEvents();
    });

    function setupEvents() {
        var mobileMenuToggle = new funkanu.ariatoggle({
            container: "header",
            triggerSelector: "#togglemain",
            target: function ($elem) {
                return $('.mobileNavigation');
            },
            toggleAction: function ($target) {
                if (mobileMenuJson === null) {
                    setupJson(toggleMainNavigation($target));
                    return;
                }

                toggleMainNavigation($target);
                $('#togglesearch').attr('aria-expanded', 'false').removeClass('active');
                $(".smallSearchArea").hide();
            }
        });

        var mobileSearchToggle = new funkanu.ariatoggle({
            container: ".mobileTriggers",
            triggerSelector: "#togglesearch",
            target: function ($elem) {
                return $('#' + $elem.attr('aria-controls'));
            },
            toggleAction: function ($target) {
                $target.slideToggle("300");
            },

            clickEvent: function ($currentTarget) {
                $currentTarget.toggleClass('active');

                $('#togglemain').attr('aria-expanded', 'false').removeClass('active');
                $('.mobileNavigation').hide();
            },
        });

        $('#MobileMenu').on('click', '.haveLinks', handleToggleChildrenClick);
        $('#MobileMenu').on('keydown', '.haveLinks', handleToggleChildrenKeyPress);
        $('#MobileMenu').on('keydown', '.childrenListContainer', handleChildrenListKeyPress);
    }

    function handleToggleChildrenKeyPress(e) {
        var $closestListItem = $($(e.currentTarget).closest('li'));
        var listItemChildrenContainerExpanded = $closestListItem.find('.haveLinks').first().attr('aria-expanded') === "true";

        var openKeyPress = e.keyCode === 13 || e.keyCode === 32 || e.keyCode === 40;
        var closeKeyPress = (e.keyCode === 27 && listItemChildrenContainerExpanded);

        if (openKeyPress || closeKeyPress) {
            toggleChildren($closestListItem);
            return false;
        }
    }

    function handleToggleChildrenClick(e) {
        e.preventDefault();

        var $listItem = $($(this).closest('li'));
        toggleChildren($listItem);
    }

    function toggleChildren($listItem) {

        var $childrenContainer = $($listItem.find('.childrenListContainer').first());
        var $toggleChildrenButton = $($listItem.find('.haveLinks').first());


        if ($listItem.hasClass('hasLoadedChildren')) {
            $childrenContainer.slideToggle("300", function () {
                setAriaAttributesForToggleChildrenButton($toggleChildrenButton);
            });
            $listItem.toggleClass('isOpen');
            return;
        }

        var id = $listItem.data('id'),
            children = mobileMenuJson[id].Children;

        $listItem.addClass('hasLoadedChildren');
        $childrenContainer.hide().append(getChildrenMarkup(children));

        $childrenContainer.slideToggle("300", function () {
            setAriaAttributesForToggleChildrenButton($toggleChildrenButton);
            $listItem.toggleClass('isOpen');


        });
    }

    function handleChildrenListKeyPress(e) {
        if (e.keyCode == 27 && $(this).is(':visible')) {
            var closestListItem = $($(this).closest('li'));
            toggleChildren(closestListItem);
            return false;
        }
    }

    function setAriaAttributesForToggleChildrenButtons() {
        $('.haveLinks').each(function () {
            setAriaAttributesForToggleChildrenButton($(this));
        });
    }

    function setAriaAttributesForToggleChildrenButton($toggleButton) {
        var $childrenContainerForButton = $toggleButton.closest('li').find('.childrenListContainer').first();

        $toggleButton.attr({
            'aria-controls': $childrenContainerForButton.attr('id'),
            'aria-expanded': $childrenContainerForButton.is(':visible') ? "true" : "false"
        });

        return $toggleButton;
    }


    function setupJson(callback) {
        var apiUrlWithParameters = getApiUrl();

        $.getJSON(apiUrlWithParameters).
            done(function (data) {
                mobileMenuJson = data;
            })
            .always(function () {

                if (typeof (callback) == "function") {
                    callback();
                }
            });
    }

    function getApiUrl() {
        var currentLanguage = $('#MobileMenu').attr('data-lang');

        if (currentLanguage == null || !currentLanguage.length) {
            return baseApiUrl;
        }

        return baseApiUrl + "?currentId=" + currentId + "&lang=" + currentLanguage;
    }

    function toggleMainNavigation($target) {
        $target.slideToggle("300");

        $('.mobileNavigation').toggleClass('show');
        $('#togglemain').toggleClass('active');
    }

    function getItemMarkup(id, backLinkMarkup, linkMarkup, nextLinkMarkup, liClasses) {
        if (currentId == id) {
            liClasses += ' current';
        }

        return '<li class="' + liClasses + '" data-id="' + id + '">' +
            '<div class=\"row\">' +
            backLinkMarkup +
            linkMarkup +
            nextLinkMarkup +
            '</div>' +
            '<div class="childrenListContainer" id="children_' + id + '"></div></li>';
    }

    function getLinkMarkup(id, href, name, hasChildren) {
        var markup = '<a href=\"' + href + '" class=\"small-' + (hasChildren ? 10 : 12) + ' columns menuLink\" data-id="' + id + '">' + name + '</a>';

        if (currentId == id) {
            markup = '<span class=\"small-' + (hasChildren ? 10 : 12) + ' columns menuLink\" data-id="' + id + '">' + name + '</span>';
        }
        return markup;
    }

    function getNextLinkMarkup(href, name) {
        var text = nextLevelText + " " + name;
        return '<button data-href="' + href + '" class="small-2 columns menuLink haveLinks">' + text + '</button>';
    }

    function getChildrenMarkup(list) {
        var items = getChildren(list),
            markup = '<ul>';

        for (var i in items) {
            markup += items[i].markup;
        }

        markup += '</ul>';

        return markup;
    }

    function getChildren(list) {
        var items = new Array();

        if (!list.length)
            return "";

        for (var i in list) {
            if (mobileMenuJson[list[i]] != null) {
                items.push({
                    id: list[i],
                    markup: getItemMarkup(
                        list[i],
                        '',
                        getLinkMarkup(list[i], mobileMenuJson[list[i]].Url, mobileMenuJson[list[i]].Name, mobileMenuJson[list[i]].Children.length),
                        mobileMenuJson[list[i]].Children.length ? getNextLinkMarkup(mobileMenuJson[list[i]].Url, mobileMenuJson[list[i]].Name) : '',
                        'menuItem'
                    )
                });
            }
        }

        return items;
    }

}(window.jQuery, window, document));



