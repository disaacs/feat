using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.Pages.Event
{
    public class EventPageController : PageController<EventPage>
    {
        public ActionResult Index(EventPage currentPage)
        {
            var model = new EventPageViewModel(currentPage);

            return View("~/Features/Pages/Event/Index.cshtml", model);
        }
    }
}