﻿using System.Web.Mvc;
using Furb.Main.Controllers;
using Furb.Main.ViewModels;

namespace Furb.Features.Pages.Standard
{
    public class StandardPageController : PageControllerBase<StandardPage>
    {
        public ActionResult Index(StandardPage currentPage)
        {
            return View("~/Features/Pages/Standard/Index.cshtml", PageViewModel.Create(currentPage));
        }
    }
}