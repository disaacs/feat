﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Furb.Features.LocalBlocks.Footer;
using Furb.Features.LocalBlocks.SiteLogoType;
using Furb.Features.LocalBlocks.SocialMediaShareLinks;
using Furb.Features.Pages.Settings;
using Furb.Features.Pages.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;
using Furb.Main.Lib.EpiExtensions.Interfaces;
using Furb.Main.Lib.MobileMenu.Interfaces;

namespace Furb.Features.Pages.Start
{
    [ContentIcon(ContentIcon.Project)]
    [ContentType(GUID = "B6F0EA2B-ACED-4E33-B527-E986C323B11E", GroupName = ContentTypeNames.Specialized, Order = 8000)]
    [AvailableContentTypes(Availability.Specific, Include = new[] { typeof(ContentPageData) }, ExcludeOn = new[] { typeof(SitePageData) })]
    [ImageUrl("~/Features/Pages/Start/Static/StartPage.png")]

    public class StartPage : SitePageData, IAlwaysVisibleInMobileMenu, IStartPage
    {
        [Display(GroupName = TabNames.Content, Order = 2100)]
        [CultureSpecific]
        public virtual ContentArea MainContentArea { get; set; }

        [Display(GroupName = TabNames.SiteSettings, Order = 2100)]
        public virtual SiteLogoTypeBlock SiteLogotype { get; set; }

        [Display(GroupName = TabNames.SiteSettings, Order = 2300)]
        public virtual SocialMediaShareLinksBlock ShareLinks { get; set; }

        [Display(GroupName = TabNames.SiteSettings, Order = 2400)]
        public virtual FooterBlock Footer { get; set; }

        [CultureSpecific]
        [AllowedTypes(typeof(SettingsPage))]
        [Display(GroupName = TabNames.SiteSettings, Order = 2600)]
        public virtual PageReference SettingsPage { get; set; }

        public virtual LinkItemCollection LinkItemCollection { get; set; }
    }
}