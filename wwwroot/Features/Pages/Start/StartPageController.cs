﻿using Furb.Main.Controllers;
using Furb.Main.ViewModels;
using System.Web.Mvc;

namespace Furb.Features.Pages.Start
{
    public class StartPageController : PageControllerBase<StartPage>
    {
        public ActionResult Index(StartPage currentPage)
        {
            //TODO: Use when forms need cleaning! 
            //new FunkaLib.Forms.Initialization.FormsEditModeCleaner().Clean();

            var model = PageViewModel.Create(currentPage);

            return View("~/Features/Pages/Start/Index.cshtml", model);
        }
    }
}