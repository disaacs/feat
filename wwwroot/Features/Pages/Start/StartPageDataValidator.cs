﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;

namespace Furb.Features.Pages.Start
{
    public class StartPageDataValidator : IValidate<StartPage>
    {
        public IEnumerable<ValidationError> Validate(StartPage instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance?.SiteLogotype== null)
            {
                return validationErrors;
            }

            if (instance.SiteLogotype.ImageUrl == null)
            {
                return validationErrors;
            }

            if (string.IsNullOrEmpty(instance.SiteLogotype.ImageAltText))
            {
                validationErrors.Add(new ValidationError
                {
                    ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/StartPage/Validation/AltTextMissing"),
                    PropertyName = instance.GetPropertyName(x => x.SiteLogotype.ImageAltText),
                    Severity = ValidationErrorSeverity.Error,
                    ValidationType = ValidationErrorType.AttributeMatched
                });
            }
            else
            {
                if (!string.IsNullOrEmpty(instance.SiteLogotype.ImageAltText))
                {
                    if (instance.SiteLogotype.ImageAltText.Replace(".", "").Replace(" ", "").Trim().Length == 0)
                    {
                        validationErrors.Add(new ValidationError
                        {
                            ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/StartPage/Validation/AltTextMissing"),
                            PropertyName = instance.GetPropertyName(x => x.SiteLogotype.ImageAltText),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        });
                    }
                    else
                    {
                        if (instance.SiteLogotype.ImageAltText.Replace(".", "").Replace(" ", "").Trim().Length < 5)
                        {
                            validationErrors.Add(new ValidationError
                            {
                                ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/StartPage/Validation/AltTextToShort"),
                                PropertyName = instance.GetPropertyName(x => x.SiteLogotype.ImageAltText),
                                Severity = ValidationErrorSeverity.Warning,
                                ValidationType = ValidationErrorType.AttributeMatched
                            });
                        }
                    }
                }
            }

            return validationErrors;
        }
    }
}