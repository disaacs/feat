using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Furb.Features.Pages.Event;
using Furb.Main.Lib.Epi.Pages.EventListPage;
using Furb.Main.Lib.EpiExtensions;

namespace Furb.Features.Blocks.EventList
{
    public class EventListBlockController : BlockController<EventListBlock>
    {
        private readonly EventListPageLogic eventListPageLogic;
        private readonly IServiceLocator serviceLocator;

        public EventListBlockController(EventListPageLogic eventListPageLogic, IServiceLocator serviceLocator)
        {
            this.eventListPageLogic = eventListPageLogic;
            this.serviceLocator = serviceLocator;
        }

        private PageData CurrentPage { get; set; }

        public override ActionResult Index(EventListBlock currentBlock)
        {
            var defualt = currentBlock.FilterList.First(x => x.IsDefault);

            return PartialView("~/Features/Blocks/EventList/Index.cshtml", CreateViewModel(currentBlock, defualt.ValueString()));
        }

        [HttpPost]
        public ActionResult Index(EventListBlock currentBlock, string timeSpan)
        {
            return PartialView("~/Features/Blocks/EventList/Index.cshtml", CreateViewModel(currentBlock, timeSpan));
        }

        private EventListBlockViewModel CreateViewModel(EventListBlock currentBlock, string timeSpan)
        {
            CurrentPage = serviceLocator.GetInstance<IPageRouteHelper>().Page;

            return new EventListBlockViewModel(currentBlock)
            {
                CurrentPage = CurrentPage.PageLink,
                Pages = FindPages(currentBlock, timeSpan),
                TimeSpan = eventListPageLogic.TimeSpanList(currentBlock.FilterList, timeSpan)
            };
        }

        private IEnumerable<EventPage> FindPages(EventListBlock currentBlock, string selectValue)
        {
            var eventPages = new List<EventPage>();
            var rootPages = currentBlock.ListRoots.GetFilteredItemsOfType<PageData>();

            foreach (var eventListPage in rootPages)
            {
                eventPages.AddRange(eventListPage.GetChildren<EventPage>());
            }

            return eventPages.Where(item => EventListPageLogic.IsVisible(item.EventInformation, selectValue)).OrderBy(x => x.EventInformation.StartTime).FilterForDisplay();
        }
    }
}