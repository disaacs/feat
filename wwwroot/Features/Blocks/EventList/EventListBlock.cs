using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Furb.Features.Blocks.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Epi.Pages.EventListPage;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes;
using LoremNET;

namespace Furb.Features.Blocks.EventList
{
    [ContentIcon(ContentIcon.Clock)]
    [ContentType(GUID = "6c44b383-90f5-4e2f-bee3-73e9d55edc3b", GroupName = ContentTypeNames.Content, Order = 1000)]
    [ImageUrl("~/Features/Blocks/EventList/Static/EventListBlock.png")]
    public class EventListBlock : SiteBlockData, IHasDemoSettings
    {
        [Required]
        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 1100)]
        public virtual string Header { get; set; }

        [Required]
        [Display(GroupName = TabNames.Content, Order = 1150)]
        public virtual ContentArea ListRoots { get; set; }

        [Required]
        [Display(GroupName = TabNames.Content, Order = 2100)]
        public virtual int MaxCount { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2200)]
        public virtual bool ShowImages { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2300)]
        [DefaultValue(false)]
        public virtual bool ShowPublishDate { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2400)]
        public virtual bool ShowIntroduction { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 2500)]
        [ShowPropertyWhenValueEquals("RootLink", true)]
        [ShowPropertyWhenValueEquals("RootLinkText", true)]
        public virtual bool ShowRootLink { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2550)]
        public virtual PageReference RootLink { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 2600)]
        public virtual string RootLinkText { get; set; }

        [Required]
        [Searchable(false)]
        [CultureSpecific(true)]
        [Display(GroupName = TabNames.Content, Order = 2800)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<TimeSpanItem>))]
        public virtual IList<TimeSpanItem> FilterList { get; set; }

        public List<string> DemoNames()
        {
            return new List<string> {"Standard", "Minimal"};
        }

        public void SetDemoSettings(int index)
        {
            Header = Lorem.Sentence(10);
            MaxCount = 5;

            switch (index)
            {
                case 1:
                    ShowIntroduction = true;
                    ShowRootLink = true;
                    RootLinkText = "Hela listan";

                    break;

                case 2:
                    ShowIntroduction = false;
                    ShowRootLink = false;

                    break;
            }
        }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            ShowIntroduction = true;

            FilterList = new List<TimeSpanItem>
            {
                new TimeSpanItem {IsDefault = true, Count = 1, Type = TimeSpanType.Month},
                new TimeSpanItem {Count = 3, Type = TimeSpanType.Month},
                new TimeSpanItem {Count = 6, Type = TimeSpanType.Month},
                new TimeSpanItem {Count = 1, Type = TimeSpanType.Year}
            };
        }
    }
}