using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.Web.Mvc;
using Furb.Features.Pages.Shared;
using Furb.Main.Lib.Epi.Blocks.PageListBlock;
using Furb.Main.Lib.Epi.Mappers;
using Furb.Main.Lib.EpiExtensions;

namespace Furb.Features.Blocks.PageListing
{
    public class PageListingBlockController : BlockController<PageListingBlock>
    {
        private readonly PageListBlockLogic pageListBlockLogic;

        public PageListingBlockController(PageListBlockLogic pageListBlockLogic)
        {
            this.pageListBlockLogic = pageListBlockLogic;
        }

        public override ActionResult Index(PageListingBlock currentBlock)
        {
            var pageTypeFilterFromProperty = currentBlock.PageTypesToList != null ? ContentTypeStringToIdsMapper.Map(currentBlock.PageTypesToList) : new List<int>();

            var pageList = pageListBlockLogic.GetFilteredPageListForContentReference(currentBlock.RootLink, currentBlock.Recursive, pageTypeFilterFromProperty, currentBlock.CategoryFilter);
            var additionalPageList = pageListBlockLogic.GetFilteredPageListForContentArea(currentBlock.AdditionalPages, pageTypeFilterFromProperty, currentBlock.CategoryFilter);

            pageList = pageList.Concat(additionalPageList);
            var newPageList = new List<PageData>();

            foreach (var page in pageList)
            {
                if (page is SitePageData sitePageData)
                {
                    if (sitePageData.HideInPageList)
                    {
                        continue;
                    }
                }
                newPageList.Add(page);
            }
            newPageList = pageListBlockLogic.Sort(newPageList, currentBlock.SortOrder).ToList();

            var model = new PageListingBlockViewModel(currentBlock)
            {
                PageList = currentBlock.MaxCount > 0 ? newPageList.Take(currentBlock.MaxCount) : newPageList,
            };

            if (currentBlock.RootLink != null)
            {
                model.RootLinkText = !string.IsNullOrEmpty(currentBlock.RootLinkText) ? currentBlock.RootLinkText : currentBlock.RootLink.GetPage().PageName;
            }

            return PartialView("~/Features/Blocks/PageListing/Index.cshtml", model);
        }
    }
}