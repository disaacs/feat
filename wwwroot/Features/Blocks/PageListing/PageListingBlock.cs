using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Filters;
using EPiServer.Shell.ObjectEditing;
using Furb.Features.Blocks.Shared;
using Furb.Features.Pages.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;
using Furb.Main.Lib.Epi.SelectionFactories;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes;

namespace Furb.Features.Blocks.PageListing
{
    [ContentIcon(ContentIcon.Thumbnails)]
    [ContentType(GUID = "ce6e7a4a-12da-4890-a3b0-b5adda2ca5fa", GroupName = ContentTypeNames.Content, Order = 1000)]
    [ImageUrl("~/Features/Blocks/PageListing/Static/PageListingBlock.png")]
    public class PageListingBlock : SiteBlockData, IHasDemoSettings
    {
        [Required]
        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 2100)]
        public virtual string Header { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2200)]
        public virtual PageReference RootLink { get; set; }

        [AllowedTypes(typeof(ContentPageData))]
        [Display(GroupName = TabNames.Content, Order = 2300)]
        public virtual ContentArea AdditionalPages { get; set; }

        [Display(GroupName = TabNames.BasicSettings, Order = 3100)]
        public virtual int MaxCount { get; set; }

        [Display(GroupName = TabNames.BasicSettings, Order = 3200)]
        public virtual bool ShowImages { get; set; }

        [Display(GroupName = TabNames.BasicSettings, Order = 3300)]
        public virtual bool ShowIntroduction { get; set; }

        [Display(GroupName = TabNames.BasicSettings, Order = 3400)]
        public virtual bool ShowPublishDate { get; set; }

        [Display(GroupName = TabNames.BasicSettings, Order = 3500)]
        public virtual bool Recursive { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.BasicSettings, Order = 3600)]
        [ShowPropertyWhenValueEquals("RootLinkText", true)]
        public virtual bool ShowRootLink { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.BasicSettings, Order = 3700)]
        public virtual string RootLinkText { get; set; }

        [UIHint("SortOrder")]
        [Display(GroupName = TabNames.BasicSettings, Order = 3800)]
        public virtual FilterSortOrder SortOrder { get; set; }

        [Display(GroupName = TabNames.BasicSettings, Order = 3900)]
        public virtual CategoryList CategoryFilter { get; set; }

        [SelectMany(SelectionFactoryType = typeof(PageTypeSelectionFactory))]
        [Display(GroupName = TabNames.BasicSettings, Order = 3950)]
        public virtual string PageTypesToList { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            MaxCount = 5;
            ShowIntroduction = true;
            ShowPublishDate = false;
            SortOrder = FilterSortOrder.PublishedDescending;
        }

        public List<string> DemoNames()
        {
            return new List<string> { "Standard", "Datum", "Bilder", "Allt", "Minimal" };
        }

        public void SetDemoSettings(int index)
        {
            Header = LoremNET.Lorem.Words(2, 3);
            MaxCount = 10;
            RootLink = ContentReference.StartPage;
            RootLinkText = "Startsidan";

            switch (index)
            {
                case 0:
                    ShowIntroduction = true;
                    ShowPublishDate = false;
                    ShowImages = false;
                    ShowRootLink = false;
                    break;

                case 1:
                    ShowIntroduction = true;
                    ShowPublishDate = true;
                    ShowImages = false;
                    ShowRootLink = false;
                    break;

                case 2:
                    ShowIntroduction = true;
                    ShowPublishDate = false;
                    ShowImages = true;
                    ShowRootLink = false;
                    break;

                case 3:
                    ShowIntroduction = true;
                    ShowPublishDate = true;
                    ShowImages = true;
                    ShowRootLink = true;
                    break;

                case 4:
                    ShowIntroduction = false;
                    ShowPublishDate = false;
                    ShowImages = false;
                    ShowRootLink = false;
                    break;
            }
        }
    }
}