﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Furb.Features.Blocks.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;
using Furb.Main.Lib.Epi.SelectionFactories;

namespace Furb.Features.Blocks.Video
{
    public enum VideoPlayer
    {
        Youtube = 0,
        Vimeo = 1
    }

    [ContentIcon(ContentIcon.ObjectVideo)]
    [ContentType(GUID = "dc561900-fcd0-4f2e-984b-d7b9a2c4648c", GroupName = ContentTypeNames.Content, Order = 1000)]
    [ImageUrl("~/Features/Blocks/Video/Static/VideoBlock.png")]
    public class VideoBlock : SiteBlockData, IHasDemoSettings
    {
        [Required]
        [SelectOne(SelectionFactoryType = typeof(EnumSelectionFactory<VideoPlayer>))]
        [Display(GroupName = TabNames.Content, Order = 1100)]
        public virtual VideoPlayer VideoPlayerType { get; set; }

        [Display(GroupName = TabNames.Content, Order = 1200)]
        public virtual string VideoHeader { get; set; }

        [Required]
        [Searchable(false)]
        [UIHint(UIHint.Textarea)]
        [Display(GroupName = TabNames.Content, Order = 1300)]
        public virtual string VideoUrl { get; set; }

        [Required]
        [Display(GroupName = TabNames.Content, Order = 1400)]
        [UIHint(UIHint.Textarea)]
        public virtual string IframeTitle { get; set; }

        [Display(GroupName = TabNames.Content, Order = 1500)]
        [UIHint(UIHint.Textarea)]
        public virtual string VideoDescription { get; set; }

        [Display(GroupName = TabNames.Content, Order = 1600)]
        public virtual int Height { get; set; }

        [Display(GroupName = TabNames.Content, Order = 1650)]
        public virtual int Width { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            VideoPlayerType = VideoPlayer.Youtube;

            Height = 360;
            Width = 640;
        }

        public List<string> DemoNames()
        {
            return new List<string> { "Vimeo", "Youtube" };
        }

        public void SetDemoSettings(int index)
        {
            switch (index)
            {
                case 0:
                    VideoPlayerType = VideoPlayer.Vimeo;
                    VideoUrl = "https://player.vimeo.com/video/143418951";
                    VideoHeader = LoremNET.Lorem.Words(2, 3);
                    IframeTitle = LoremNET.Lorem.Words(2, 3);
                    VideoDescription = LoremNET.Lorem.Words(5, 10);

                    break;
                case 1:
                    VideoPlayerType = VideoPlayer.Youtube;
                    VideoUrl = "https://www.youtube.com/watch?v=EY8FS9NhrBc";
                    VideoHeader = LoremNET.Lorem.Words(2, 3);
                    IframeTitle = LoremNET.Lorem.Words(2, 3);
                    VideoDescription = LoremNET.Lorem.Words(5, 10);
                    break;
            }
        }
    }
}