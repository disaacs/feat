using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.Blocks.Video
{
    public class VideoBlockController : BlockController<VideoBlock>
    {
        public override ActionResult Index(VideoBlock currentBlock)
        {
            var model = new VideoBlockViewModel(currentBlock);

            return PartialView("~/Features/Blocks/Video/Index.cshtml", model);
        }
    }
}