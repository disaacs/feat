﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;

namespace Furb.Features.Blocks.ContentImage
{
    public class ContentImageBlockDataValidator : IValidate<ContentImageBlock>
    {
        public IEnumerable<ValidationError> Validate(ContentImageBlock instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance?.Image == null)
            {
                return validationErrors;
            }

            if (instance.DecorativeImage)
            {
                return validationErrors;
            }

            if (string.IsNullOrEmpty(instance.AltText))
            {
                validationErrors.Add(new ValidationError
                {
                    ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/ContentImageBlock/Validation/AltTextMissing"),
                    PropertyName = instance.GetPropertyName(x => x.AltText),
                    Severity = ValidationErrorSeverity.Error,
                    ValidationType = ValidationErrorType.AttributeMatched
                });
            }
            else
            {
                if (!string.IsNullOrEmpty(instance.AltText))
                {
                    if (instance.AltText.Replace(".", "").Replace(" ", "").Trim().Length == 0)
                    {
                        validationErrors.Add(new ValidationError
                        {
                            ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/ContentImageBlock/Validation/AltTextMissing"),
                            PropertyName = instance.GetPropertyName(x => x.AltText),
                            Severity = ValidationErrorSeverity.Error,
                            ValidationType = ValidationErrorType.AttributeMatched
                        });
                    }
                    else
                    {
                        if (instance.AltText.Replace(".", "").Replace(" ", "").Trim().Length < 5)
                        {
                            validationErrors.Add(new ValidationError
                            {
                                ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/ContentImageBlock/Validation/AltTextToShort"),
                                PropertyName = instance.GetPropertyName(x => x.AltText),
                                Severity = ValidationErrorSeverity.Warning,
                                ValidationType = ValidationErrorType.AttributeMatched
                            });
                        }
                    }
                }
            }

            return validationErrors;
        }
    }
}