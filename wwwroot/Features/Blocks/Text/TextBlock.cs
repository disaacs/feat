using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Furb.Features.Blocks.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;
using LoremNET;

namespace Furb.Features.Blocks.Text
{
    [ContentIcon(ContentIcon.Pen)]
    [ContentType(GUID = "c0110edf-95c7-48e3-935f-cd701538901a", GroupName = ContentTypeNames.Content)]
    [ImageUrl("~/Features/Blocks/Text/Static/TextBlock.png")]
    public class TextBlock : SiteBlockData, IHasDemoSettings
    {
        [Display(GroupName = TabNames.Content, Order = 100)]
        public virtual XhtmlString Text { get; set; }

        public List<string> DemoNames()
        {
            return new List<string> {"TextBlock"};
        }

        public void SetDemoSettings(int index)
        {
            Text = new XhtmlString(Lorem.Sentence(20, 50));
        }
    }
}