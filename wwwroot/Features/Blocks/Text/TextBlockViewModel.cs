using Furb.Main.ViewModels;

namespace Furb.Features.Blocks.Text
{
    public class TextBlockViewModel : BlockViewModel<TextBlock>
    {
        public TextBlockViewModel(TextBlock currentBlock)
            : base(currentBlock)
        {
        }
    }
}