﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;

namespace Furb.Features.LocalBlocks.EventInformation
{
    public class EventInformationBlockValidator : IValidate<EventInformationBlock>
    {
        public IEnumerable<ValidationError> Validate(EventInformationBlock instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance.StartTime > instance.EndTime)
            {
                validationErrors.Add(new ValidationError
                {
                    ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/EventPage/Validation/EndTimeBeforeStartTime"),
                    PropertyName = instance.GetPropertyName(x => x.EndTime),
                    Severity = ValidationErrorSeverity.Error,
                    ValidationType = ValidationErrorType.AttributeMatched
                });
            }

            return validationErrors;
        }
    }
}