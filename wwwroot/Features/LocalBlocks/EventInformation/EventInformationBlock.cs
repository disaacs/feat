﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Furb.Features.Blocks.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Features.LocalBlocks.EventInformation
{
    [ContentType(GUID = "aab50f5c-f887-43da-beab-8ca7a70e3955", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    public class EventInformationBlock : SiteBlockData, IHasEventDate
    {
        [Required(AllowEmptyStrings = false)]
        [Display(Order = 2300)]
        public virtual DateTime StartTime { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Order = 2350)]
        public virtual DateTime EndTime { get; set; }

        [UIHint(UIHint.Textarea)]
        [Display(Order = 2400)]
        public virtual string EventVenue { get; set; }

        [UIHint(UIHint.Textarea)]
        [Display(Order = 2450)]
        public virtual string City { get; set; }
    }
}