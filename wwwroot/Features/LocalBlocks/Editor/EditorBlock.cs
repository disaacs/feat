﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Furb.Features.Blocks.Shared;
using Furb.Main;

namespace Furb.Features.LocalBlocks.Editor
{
    [ContentType(GUID = "0CB6DBEC-C7D1-48FA-BD19-A0FA9187E370", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    public class EditorBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(Order = 2100)]
        public virtual XhtmlString Text { get; set; } 
    }
}