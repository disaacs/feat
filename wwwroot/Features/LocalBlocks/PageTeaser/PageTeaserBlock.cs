﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Furb.Features.Blocks.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Features.LocalBlocks.PageTeaser
{
    [ContentType(GUID = "b9dd6266-4040-439c-90aa-8335129ef0b5", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    public class PageTeaserBlock : SiteBlockData, ITeaserBlock
    {
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(Order = 2100)]
        public virtual string TeaserText { get; set; }

        [CultureSpecific]
        [Display(Order = 2200)]
        public virtual string LinkText { get; set; }
    }
}