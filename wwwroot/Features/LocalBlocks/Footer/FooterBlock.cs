﻿using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Furb.Features.LocalBlocks.Editor;
using Furb.Main;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Furb.Features.Blocks.Shared;

namespace Furb.Features.LocalBlocks.Footer
{
    [ContentType(GUID = "6612043D-93CA-4C84-8E88-DFE4FACB1A25", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    public class FooterBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(Order = 2100)]
        public virtual string FooterColumn1Header { get; set; }

        [Display(Order = 2110)]
        public virtual EditorBlock FooterColumn1 { get; set; }

        [CultureSpecific]
        [Display(Order = 2200)]
        public virtual string FooterColumn2Header { get; set; }

        [CultureSpecific]
        [Display(Order = 2210)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<SocialLinkItem>))]
        public virtual IList<SocialLinkItem> SocialLinkList { get; set; }

        [CultureSpecific]
        [Display(Order = 2300)]
        public virtual string FooterColumn3Header { get; set; }

        [CultureSpecific]
        [Display(Order = 2310)]
        public virtual LinkItemCollection FooterColumn3 { get; set; }
    }
}