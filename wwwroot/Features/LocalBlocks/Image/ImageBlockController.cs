﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.LocalBlocks.Image
{
    public class ImageBlockController : BlockController<ImageBlock>
    {
        public override ActionResult Index(ImageBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/Image/Index.cshtml", currentBlock);
        }
    }
}
