﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.LocalBlocks.SiteLogoType
{
    public class SiteLogoTypeBlockController : BlockController<SiteLogoTypeBlock>
    {
        public override ActionResult Index(SiteLogoTypeBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/SiteLogoType/Index.cshtml", currentBlock);
        }
    }
}
