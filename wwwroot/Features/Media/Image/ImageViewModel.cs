﻿using EPiServer.Core;

namespace Furb.Features.Media.Image
{
    public class ImageViewModel
    {
        public ContentReference ContentReference { get; set; }

        public string Url { get; set; }

        public string Name { get; set; }

        public string AltText { get; set; }

        public string Copyright { get; set; }
    }
}