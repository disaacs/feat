﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Features.Media.Image
{
    [ContentType(GUID = "0A89E464-56D4-449F-AEA8-2BF774AB8730", GroupName = ContentTypeNames.Content, Order = 1000)]
    [MediaDescriptor(ExtensionString = "jpg,jpeg,jpe,ico,gif,bmp,png,svg")]
    public class ImageFile : ImageData, IHasAltText, IHasDescription
    {
        [CultureSpecific]
        public virtual string Copyright { get; set; }

        [CultureSpecific]
        public virtual string AltText { get; set; }

        [CultureSpecific]
        public virtual string Description { get; set; }
    }
}