﻿using System.Linq;
using System.Web.Routing;
using EPiServer;
using EPiServer.Core;
using Furb.Features.Pages.Start;
using Furb.Main.Lib.EpiExtensions;

namespace Furb.Main
{
    public class PageViewContextFactory
    {
        private readonly IContentLoader contentLoader;

        public PageViewContextFactory(IContentLoader contentLoader)
        {
            this.contentLoader = contentLoader;
        }

        public virtual LayoutModel CreateLayoutModel(ContentReference currentContentLink, RequestContext requestContext)
        {
            var settingsPage = ContentReferenceExtended.SettingsPage;
            var mainStartPage = (StartPage) ContentReference.StartPage.GetPage();

            var layoutModel = new LayoutModel
            {
                SiteSettingsPage = settingsPage,
                LoggedIn = requestContext.HttpContext.User.Identity.IsAuthenticated,
                SiteLogotype = mainStartPage.SiteLogotype,
                SocialMediaShareLinks = mainStartPage.ShareLinks,
                SearchPageRouteValues = requestContext.GetPageRoute(ContentReference.StartPage),
                Footer = mainStartPage.Footer
            };

            return layoutModel;
        }

        public virtual IContent GetSection(ContentReference contentLink)
        {
            var currentContent = contentLoader.Get<IContent>(contentLink);

            if (currentContent.ParentLink != null && currentContent.ParentLink.CompareToIgnoreWorkID(contentLink.GetStartPage<StartPage>().ContentLink))
            {
                return currentContent;
            }

            return contentLoader.GetAncestors(contentLink).OfType<PageData>().SkipWhile(x => x.ParentLink == null || !x.ParentLink.CompareToIgnoreWorkID(contentLink.GetStartPage<StartPage>().ContentLink)).FirstOrDefault();
        }
    }
}