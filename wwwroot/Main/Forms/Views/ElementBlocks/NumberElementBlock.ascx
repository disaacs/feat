<%--
    =====================================
    Version: 4.28.0.0. Modified: 20200226
    =====================================
--%>

<%@ Import Namespace="System.Web.Mvc" %>
<%@ Import Namespace="EPiServer.Forms.Helpers.Internal" %>
<%@ Import Namespace="EPiServer.Forms.Implementation.Elements" %>
<%@ Control Language="C#" Inherits="ViewUserControl<NumberElementBlock>" %>

<%  var formElement = Model.FormElement; 
    var labelText = Model.Label;
    var cssClasses = Model.GetValidationCssClasses();
%>

<% using(Html.BeginElement(Model, new { @class="FormTextbox FormTextbox--Number" + cssClasses, @data_f_type="textbox", data_f_modifier="number" })) { %>
    <label for="<%: formElement.Guid %>" class="Form__Element__Caption"><%: labelText %></label>
    <input name="<%: formElement.ElementName %>" id="<%: formElement.Guid %>" type="number"
        <% if (!string.IsNullOrWhiteSpace(Model.PlaceHolder)) { %>
            placeholder="<%: Model.PlaceHolder %>"
        <% } %>
        class="FormTextbox__Input" <%= Model.AttributesString %> data-f-datainput
        value="<%: Model.GetDefaultValue() %>" aria-describedby="<%: Util.GetAriaDescribedByElementName(formElement.ElementName) %>"
        aria-invalid="<%: Util.GetAriaInvalidByValidationCssClasses(cssClasses) %>"/>
    <%= Html.ValidationMessageFor(Model) %>
    <%= Model.RenderDataList() %>
<% } %>
