﻿namespace Furb.Main
{
    public static class TabNames
    {
        public const string MetaData = "Metadata";
        public const string BasicSettings = "BasicSettings";
        public const string SiteSettings = "SiteSettings";
        public const string PageSettings = "PageSettings";
        public const string MoreContent = "MoreContent";
        public const string Conditions = "Conditions";
        public const string Unknown = "Unknown";
        public const string Content = "Content";
        public const string PageHead = "Header";
    }
}