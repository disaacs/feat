﻿using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Shell.Navigation;
using EPiServer.Web;
using EPiServer.Web.PageExtensions;

namespace Furb.Main.Initialization
{
    [ServiceConfiguration(typeof(IQuickNavigatorItemProvider))]
    public class QuickNavigatorItemProvider : IQuickNavigatorItemProvider
    {
        public IDictionary<string, QuickNavigatorMenuItem> GetMenuItems(ContentReference currentContent)
        {
            var menuItems = new Dictionary<string, QuickNavigatorMenuItem>();

            if (PrincipalInfo.HasAdminAccess)
            {
                menuItems.Add("Admin", new QuickNavigatorMenuItem("/shell/cms/menu/admin", UriSupport.ResolveUrlFromUIAsRelativeOrAbsolute("Admin/default.aspx"), null, "true", null));
            }

            if (PrincipalInfo.HasAdminAccess)
            {
                menuItems.Add("Reports", new QuickNavigatorMenuItem("/shell/cms/menu/reportcenter", UriSupport.ResolveUrlFromUIAsRelativeOrAbsolute("Report/default.aspx"), null, "true", null));
            }

            if (PrincipalInfo.Current.Principal.IsInRole("CmsAdmins") || PrincipalInfo.Current.Principal.IsInRole("VisitorGroupAdmins"))
            {
                menuItems.Add("Visitor Groups", new QuickNavigatorMenuItem("/shell/cms/visitorgroups/index/name", UriSupport.ResolveUrlFromUIAsRelativeOrAbsolute("VisitorGroups"), null, "true", null));
            }

            return menuItems;
        }

        public int SortOrder => int.MaxValue - 10;
    }

    [MenuProvider]
    public class CmsMenuProvider : IMenuProvider
    {
        public IEnumerable<MenuItem> GetMenuItems()
        {
            var menuItems = new List<MenuItem>
            {
                new UrlMenuItem("Admin", MenuPaths.Global + "/globalLink", UriSupport.ResolveUrlFromUIAsRelativeOrAbsolute("Admin/Default.aspx"))
                {
                    SortIndex = 200, IsAvailable = request => PrincipalInfo.HasAdminAccess
                },
                new UrlMenuItem("Reports", MenuPaths.Global + "/globalLink", UriSupport.ResolveUrlFromUIAsRelativeOrAbsolute("Report/default.aspx"))
                {
                    SortIndex = 250, IsAvailable = request => PrincipalInfo.HasAdminAccess
                },
                new UrlMenuItem("Visitor Groups", MenuPaths.Global + "/globalLink", UriSupport.ResolveUrlFromUIAsRelativeOrAbsolute("VisitorGroups"))
                {
                    SortIndex = 250, IsAvailable = request => PrincipalInfo.Current.Principal.IsInRole("CmsAdmins") || PrincipalInfo.Current.Principal.IsInRole("VisitorGroupAdmins")
                },
                new UrlMenuItem("EPi Documentation", MenuPaths.User + "/episerver2", "https://world.episerver.com/documentation/")
                {
                    SortIndex = SortIndex.Last + 10
                }
            };

            return menuItems;
        }
    }
}