﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Furb.Main.Initialization
{
    [EditorDescriptorRegistration(TargetType = typeof(ContentData))]
    public class MoveToCustomTabsEditorDescriptor : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            if (metadata.Properties.Any(item => item.ShowForEdit && ((ExtendedMetadata) item).GroupName == TabNames.PageSettings))
            {
                // ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
                foreach (ExtendedMetadata property in metadata.Properties.Where(item => item.ShowForEdit))
                {
                    if (property.GroupName == SystemTabNames.Settings || property.GroupName == SystemTabNames.Content)
                    {
                        property.GroupName = TabNames.PageSettings;
                    }
                }
            }
            else
            {
                // ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
                foreach (ExtendedMetadata property in metadata.Properties.Where(item => item.ShowForEdit))
                {
                    if (property.GroupName == SystemTabNames.Content)
                    {
                        property.GroupName = SystemTabNames.Settings;
                    }
                }
            }
        }
    }
}