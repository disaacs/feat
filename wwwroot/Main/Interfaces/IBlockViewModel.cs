﻿using Furb.Features.Blocks.Shared;

namespace Furb.Main.Interfaces
{
    public interface IBlockViewModel<out T> where T : SiteBlockData
    {
    }
}