﻿using System.Web.Routing;
using Furb.Features.LocalBlocks.Footer;
using Furb.Features.LocalBlocks.SiteLogoType;
using Furb.Features.LocalBlocks.SocialMediaShareLinks;
using Furb.Features.Pages.Settings;

namespace Furb.Main
{
    public class LayoutModel
    {
        public bool LoggedIn { get; set; }

        public SiteLogoTypeBlock SiteLogotype { get; set; }

        public RouteValueDictionary SearchPageRouteValues { get; set; }

        public SocialMediaShareLinksBlock SocialMediaShareLinks { get; set; }

        public FooterBlock Footer { get; set; }

        public SettingsPage SiteSettingsPage { get; set; }

        public string ColorTheme { get; set; }
    }
}