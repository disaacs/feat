﻿using System;
using EPiServer.Web.Routing;
using Furb.Features.Media.Image;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Main.Mappers
{
    public class ImageFileToImageViewModelMapper : IMapToNew<ImageFile, ImageViewModel>
    {
        private readonly UrlResolver urlResolver;
        public ImageFileToImageViewModelMapper(UrlResolver urlResolver)
        {
            this.urlResolver = urlResolver;
        }

        public ImageViewModel Map(ImageFile source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new ImageViewModel
            {
                Url = urlResolver.GetUrl(source.ContentLink),
                Name = source.Name,
                AltText = source.AltText,
                Copyright = source.Copyright,
                ContentReference = source.ContentLink
            };
        }
    }
}