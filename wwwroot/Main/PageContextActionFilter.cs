﻿using System.Web.Mvc;
using EPiServer.Web.Routing;
using Furb.Features.Pages.Shared;
using Furb.Main.Interfaces;

namespace Furb.Main
{
    public class PageContextActionFilter : IResultFilter
    {
        private readonly PageViewContextFactory contextFactory;

        public PageContextActionFilter(PageViewContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model;

            if (viewModel is IPageViewModel<SitePageData> model)
            {
                var currentContentLink = filterContext.RequestContext.GetContentLink();

                var layoutModel = model.Layout ?? contextFactory.CreateLayoutModel(currentContentLink ?? model.CurrentPage.ContentLink, filterContext.RequestContext);

                // ReSharper disable once SuspiciousTypeConversion.Global
                var layoutController = filterContext.Controller as IModifyLayout;
                layoutController?.ModifyLayout(layoutModel);

                model.Layout = layoutModel;

                if (model.Section == null)
                {
                    model.Section = contextFactory.GetSection(currentContentLink ?? model.CurrentPage.ContentLink);
                }
            }
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }
    }
}
