﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;

namespace Furb.Main.Lib.EpiExtensions
{
    public static class ContentAreaExtensions
    {
        public static IEnumerable<T> GetFilteredItemsOfType<T>(this EPiServer.Core.ContentArea contentArea)
        {
            if (contentArea == null)
            {
                return new List<T>();
            }

            var contentAreaItems = contentArea.FilteredItems.Select(c => c.GetContent());
            return contentAreaItems.Where(item => item is T).Cast<T>();
        }
    }
}