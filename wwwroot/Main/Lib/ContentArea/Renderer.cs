using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EPiServer.Web.Mvc.Html;

namespace Furb.Main.Lib.ContentArea
{
    public class Renderer : ContentAreaRenderer
    {
        public Renderer(IContentRenderer contentRenderer, TemplateResolver templateResolver, IContentAreaItemAttributeAssembler attributeAssembler, IContentRepository contentRepository, IContentAreaLoader contentAreaLoader)
            : base(contentRenderer, templateResolver, attributeAssembler, contentRepository, contentAreaLoader)
        {
        }

        protected override string GetContentAreaItemCssClass(HtmlHelper htmlHelper, ContentAreaItem contentAreaItem)
        {
            var tag = GetContentAreaItemTemplateTag(htmlHelper, contentAreaItem) ?? Tags.FullWidth;
            return $"block {GetTypeSpecificCssClasses(contentAreaItem, ContentRepository)} {GetCssClassForTag(tag)} {GetAllTagsforTag(tag) + " columns"}";
        }

        protected override void RenderContentAreaItems(HtmlHelper htmlHelper, IEnumerable<ContentAreaItem> contentAreaItems)
        {
            var rowTag = new TagBuilder("div");
            rowTag.AddCssClass("row");
            var numberOfColumns = 0;
            const int maxNumberOfColumns = 12; 

            htmlHelper.ViewContext.Writer.Write(rowTag.ToString(TagRenderMode.StartTag));
            var list = contentAreaItems.ToList();
            foreach (var contentAreaItem in list)
            {
                if (numberOfColumns >= maxNumberOfColumns)
                {
                    htmlHelper.ViewContext.Writer.Write(rowTag.ToString(TagRenderMode.StartTag));
                    numberOfColumns = 0;
                }
                if ((list.IndexOf(contentAreaItem) == list.Count - 1) && (numberOfColumns < maxNumberOfColumns))
                {
                    RenderContentAreaItem(htmlHelper, contentAreaItem, GetContentAreaItemTemplateTag(htmlHelper, contentAreaItem), GetContentAreaItemHtmlTag(htmlHelper, contentAreaItem), GetContentAreaItemCssClass(htmlHelper, contentAreaItem) + " end");
                }
                else
                {
                    RenderContentAreaItem(htmlHelper, contentAreaItem, GetContentAreaItemTemplateTag(htmlHelper, contentAreaItem), GetContentAreaItemHtmlTag(htmlHelper, contentAreaItem), GetContentAreaItemCssClass(htmlHelper, contentAreaItem));
                }
                var tag = GetContentAreaItemTemplateTag(htmlHelper, contentAreaItem);
                if (tag != null)
                {
                    numberOfColumns += int.Parse(Regex.Match(tag, @"\d+").Value);
                }
                else
                {
                    numberOfColumns += maxNumberOfColumns;
                }

                if (numberOfColumns >= maxNumberOfColumns)
                {
                    htmlHelper.ViewContext.Writer.Write(rowTag.ToString(TagRenderMode.EndTag));
                }
            }
            if (numberOfColumns < maxNumberOfColumns)
            {
                htmlHelper.ViewContext.Writer.Write(rowTag.ToString(TagRenderMode.EndTag));
            }
        }

        private static string GetAllTagsforTag(string tag)
        {
            if (string.IsNullOrEmpty(tag))
            {
                return "";
            }
            switch (tag.ToLower())
            {
                case "medium-12":
                    return "medium-12 small-12";

                case "medium-8":
                    return "medium-8 small-12";

                case "medium-6":
                    return "medium-6 small-12";

                case "medium-4":
                    return "medium-4 small-12";

                case "medium-3":
                    return "medium-3 small-6";

                default:
                    return string.Empty;
            }
        }

        private static string GetCssClassForTag(string tagName)
        {
            if (string.IsNullOrEmpty(tagName))
            {
                return string.Empty;
            }

            switch (tagName.ToLower())
            {
                case "medium-12":
                    return "full";

                case "medium-8":
                    return "wide";

                case "medium-6":
                    return "half";

                case "medium-4":
                    return "third";

                case "medium-3":
                    return "quarter";

                default:
                    return string.Empty;
            }
        }

        private static string GetTypeSpecificCssClasses(ContentAreaItem contentAreaItem, IContentRepository contentRepository)
        {
            var content = contentAreaItem.GetContent();
            var cssClass = content?.GetOriginalType().Name.ToLowerInvariant() ?? string.Empty;

            var customClassContent = content as ICustomCssInContentArea;
            if (!string.IsNullOrWhiteSpace(customClassContent?.ContentAreaCssClass))
            {
                cssClass += $"{customClassContent.ContentAreaCssClass}";
            }

            return cssClass;
        }
    }
}