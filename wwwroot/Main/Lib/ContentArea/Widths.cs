namespace Furb.Main.Lib.ContentArea
{
    public static class Widths
    {
        public const int FullWidth = 12;
        public const int TwoThirdsWidth = 8;
        public const int HalfWidth = 6;
        public const int OneThirdWidth = 4;
        public const int OneFourthWidth = 3;
    }
}