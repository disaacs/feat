﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using InitializationModule = EPiServer.Web.InitializationModule;

namespace Furb.Main.Lib.ContentArea
{
    [InitializableModule]
    [ModuleDependency(typeof(InitializationModule))]
    public class DisplayRegistryInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            if (context.HostType != HostType.WebApplication)
            {
                return;
            }

            var options = ServiceLocator.Current.GetInstance<DisplayOptions>();

            options
                .Add("full", "/displayoptions/full", Tags.FullWidth, "", "epi-icon__layout--full")
                .Add("wide", "/displayoptions/wide", Tags.TwoThirdsWidth, "", "epi-icon__layout--two-thirds")
                .Add("half", "/displayoptions/half", Tags.HalfWidth, "", "epi-icon__layout--half")
                .Add("narrow", "/displayoptions/narrow", Tags.OneThirdWidth, "", "epi-icon__layout--one-third")
                .Add("quarter", "/displayoptions/quarter", Tags.OneFourthWidth, "", "epi-icon__layout--one-fourth");
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}