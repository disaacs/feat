namespace Furb.Main.Lib.ContentArea
{
    public static class Tags
    {
        public const string FullWidth = "medium-12";
        public const string TwoThirdsWidth = "medium-8";
        public const string HalfWidth = "medium-6";
        public const string OneThirdWidth = "medium-4";
        public const string OneFourthWidth = "medium-3";
        public const string NoRenderer = "norenderer";
    }
}