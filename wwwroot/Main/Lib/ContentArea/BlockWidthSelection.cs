﻿using System;
using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Furb.Main.Lib.ContentArea
{
    [EditorDescriptorRegistration(TargetType = typeof (string), UIHint = "CustomBlockWidthSelection")]
    public class BlockWidthSelection : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            SelectionFactoryType = typeof (BlockWidthSelectionFactory);
            ClientEditingClass = "epi.cms.contentediting.editors.SelectionEditor";

            base.ModifyMetadata(metadata, attributes);
        }
    }

    public class BlockWidthSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var locations = new List<SelectItem>
            {
                new SelectItem {Value = Widths.FullWidth, Text = "Fullbredd"},
                new SelectItem {Value = Widths.TwoThirdsWidth, Text = "Två tredjedelar"},
                new SelectItem {Value = Widths.HalfWidth, Text = "Halvbredd"},
                new SelectItem {Value = Widths.OneThirdWidth, Text = "En Tredjedel"}
            };

            return locations;
        }
    }
}