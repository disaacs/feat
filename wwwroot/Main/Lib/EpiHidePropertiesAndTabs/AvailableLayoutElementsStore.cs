using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Shell.Services.Rest;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs
{
    [RestStore("availableLayoutelementsstore")]
    public class AvailableLayoutElementsStore : RestControllerBase
    {
        private readonly IContentLoader contentLoader;
        private readonly ILayoutVisibilityResolver layoutVisibilityResolver;

        // ReSharper disable once SuggestBaseTypeForParameter, Have to be CompositeLayoutResolver or else it will only check default! 
        public AvailableLayoutElementsStore(IContentLoader contentLoader, CompositeLayoutResolver layoutVisibilityResolver)
        {
            this.contentLoader = contentLoader;
            this.layoutVisibilityResolver = layoutVisibilityResolver;
        }

        [HttpGet]
        public RestResultBase Get(ContentReference id)
        {
            var content = contentLoader.Get<IContent>(id);

            var hiddenTabs = layoutVisibilityResolver.GetHiddenTabs(content);
            var hiddenProperties = layoutVisibilityResolver.GetHiddenProperties(content);

            return new RestResult
            {
                Data = new
                {
                    HiddenTabs = hiddenTabs,
                    HiddenProperties = hiddenProperties
                }
            };
        }
    }
}
