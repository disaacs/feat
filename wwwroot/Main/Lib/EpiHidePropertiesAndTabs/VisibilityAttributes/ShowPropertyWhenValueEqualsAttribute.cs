using System;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class ShowPropertyWhenValueEqualsAttribute : PropertyVisibilityAttributeBase
    {
        public ShowPropertyWhenValueEqualsAttribute(string propertyName, object value) : base(propertyName, value)
        {
        }
    }
}