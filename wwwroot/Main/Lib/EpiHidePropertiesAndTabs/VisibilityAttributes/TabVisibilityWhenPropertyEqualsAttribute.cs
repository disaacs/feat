using System;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class ShowTabWhenPropertyEqualsAttribute : TabVisibilityAttributeBase
    {
        public ShowTabWhenPropertyEqualsAttribute(string tabName, object value) : base(tabName, value)
        {
        }
    }
}