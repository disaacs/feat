using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver
{
    [ServiceConfiguration(typeof(ILayoutVisibilityResolver))]
    public class AttributeBasedLayoutVisibilityResolver : ILayoutVisibilityResolver
    {
        public IEnumerable<string> GetHiddenTabs(IContent content)
        {
            var result = new List<string>();

            var modelType = content.GetOriginalType();

            foreach (var property in modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var attributes = property.GetCustomAttributes();

                foreach (var attribute in attributes.OfType<TabVisibilityAttributeBase>())
                {
                    if (attribute is ShowTabWhenPropertyEqualsAttribute showTabWhenPropertyEqualsAttribute)
                    {
                        if (showTabWhenPropertyEqualsAttribute.Value.Equals(content.Property[property.Name].Value) == false)
                        {
                            result.Add(showTabWhenPropertyEqualsAttribute.TabName);
                        }

                        continue;
                    }

                    if (attribute is HideTabWhenPropertyEqualsAttribute hideTabWhenPropertyEqualsAttribute)
                    {
                        if (hideTabWhenPropertyEqualsAttribute.Value.Equals(content.Property[property.Name].Value))
                        {
                            result.Add(hideTabWhenPropertyEqualsAttribute.TabName);
                        }
                    }
                }
            }

            return result.GroupBy(x => x).Select(x => x.FirstOrDefault());
        }

        public IEnumerable<string> GetHiddenProperties(IContent content)
        {
            var result = new List<string>();

            var modelType = content.GetOriginalType();

            foreach (var property in modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var attributes = property.GetCustomAttributes();

                foreach (var attribute in attributes.OfType<PropertyVisibilityAttributeBase>())
                {
                    if (attribute is ShowPropertyWhenValueEqualsAttribute showPropertyWhenValueEqualsAttribute)
                    {
                        if (showPropertyWhenValueEqualsAttribute.Value.Equals(content.Property[property.Name].Value) == false)
                        {
                            result.Add(showPropertyWhenValueEqualsAttribute.PropertyName);
                        }

                        continue;
                    }

                    if (attribute is HidePropertyWhenValueEquals hidePropertyWhenValueEquals)
                    {
                        if (hidePropertyWhenValueEquals.Value.Equals(content.Property[property.Name].Value))
                        {
                            result.Add(hidePropertyWhenValueEquals.PropertyName);
                        }
                    }
                }
            }

            return result.GroupBy(x => x).Select(x => x.FirstOrDefault());
        }
    }
}