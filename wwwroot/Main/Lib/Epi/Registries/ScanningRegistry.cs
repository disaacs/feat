﻿using Furb.Main.Lib.Epi.Interfaces;
using StructureMap;

namespace Furb.Main.Lib.Epi.Registries
{
    public class ScanningRegistry : Registry
    {
        public ScanningRegistry()
        {
            Scan(cfg =>
            {
                cfg.AssemblyContainingType(typeof(IMapToNew<,>));
                cfg.SingleImplementationsOfInterface();
                cfg.ConnectImplementationsToTypesClosing(typeof(IMapToNew<,>));
            });
        }
    }
}