﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;

namespace Furb.Main.Lib.Epi.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class DataAnnotationsModelValidatorInitialization : IInitializableModule
    {
        private static bool initialized;

        public void Initialize(InitializationEngine context)
        {
            if (initialized)
            {
                return;
            }

            initialized = true;
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}