﻿using EPiServer.Core;
using EPiServer.Framework.Serialization;
using EPiServer.Framework.Serialization.Internal;
using EPiServer.ServiceLocation;

namespace Furb.Main.Lib.Epi
{
    public class PropertyListBase<T> : PropertyList<T>
    {
        public PropertyListBase()
        {
            objectSerializer = objectSerializerFactory.Service.GetSerializer("application/json");
        }

        private Injected<ObjectSerializerFactory> objectSerializerFactory;

        private readonly IObjectSerializer objectSerializer;

        protected override T ParseItem(string value)
        {
            return objectSerializer.Deserialize<T>(value);
        }
    }
}