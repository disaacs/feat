﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;

namespace Furb.Main.Lib.Epi.SelectionFactories
{
    [SelectionFactoryRegistration]
    public class CategorySelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var selectItems = new List<ISelectItem>();
            var categoryRepository = ServiceLocator.Current.GetInstance<CategoryRepository>();
            var categories = categoryRepository.GetRoot().Categories;

            if (categories != null)
            {
                selectItems.AddRange(categories.Select(cat => new SelectItem
                {
                    Text = cat.LocalizedDescription,
                    Value = cat.ID
                }));
            }

            return selectItems;
        }
    }
}