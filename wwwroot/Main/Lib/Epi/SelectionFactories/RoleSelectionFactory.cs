﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using EPiServer.Shell.ObjectEditing;

namespace Furb.Main.Lib.Epi.SelectionFactories
{
    [SelectionFactoryRegistration]
    public class RoleSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var selectItems = new List<ISelectItem>();
            var roles = Roles.GetAllRoles();

            if (roles != null)
            {
                selectItems.AddRange(roles.Select(role => new SelectItem
                {
                    Text = role,
                    Value = role
                }));
            }

            return selectItems;
        }
    }
}