﻿using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Main.Lib.Epi.Helpers
{
    public static class EventHelpers
    {
        public static string TimeSpan(IHasEventDate eventInformation)
        {
            if (eventInformation != null)
            {
                if (eventInformation.StartTime.Date.Equals(eventInformation.EndTime.Date))
                {
                    return eventInformation.StartTime.ToString("d MMMM") + '\n' + eventInformation.StartTime.ToString(" HH.mm") + "-" + eventInformation.EndTime.ToString("HH.mm");
                }

                return eventInformation.StartTime.ToString("d MMMM HH.mm") + " - " + '\n' + eventInformation.EndTime.ToString("d MMMM HH.mm");
            }

            return string.Empty;
        }
    }
}