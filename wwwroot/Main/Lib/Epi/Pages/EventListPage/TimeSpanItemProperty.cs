﻿using EPiServer.PlugIn;

namespace Furb.Main.Lib.Epi.Pages.EventListPage
{
    [PropertyDefinitionTypePlugIn]
    public class TimeSpanItemProperty : PropertyListBase<TimeSpanItem>
    {
    }
}