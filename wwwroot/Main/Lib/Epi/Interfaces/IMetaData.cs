﻿namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IMetaData
    {
        string MetaTitle { get; set; }

        string MetaDescription { get; set; }

        bool DisableIndexing { get; set; }
    }
}