﻿using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IContentBlock
    {
        string Intro { get; set; }

        XhtmlString MainBody { get; set; }
    }
}