using System.Collections.Generic;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHasDemoSettings
    {
        List<string> DemoNames();

        void SetDemoSettings(int index);
    }
}