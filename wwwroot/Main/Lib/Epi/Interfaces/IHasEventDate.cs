﻿using System;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHasEventDate
    {
        DateTime StartTime { get; set; }

        DateTime EndTime { get; set; }
    }

    public interface IHasEvent<T> where T : IHasEventDate
    {
        T EventInformation { get; set; }
    }
}