﻿using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHasMainContent<T> where T : BlockData, IContentBlock
    {
        T MainContentBlock { get; set; }
    }
}