﻿using System.ComponentModel.DataAnnotations;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface ITeaserBlockWithLineBreak : ITeaserBlock
    {
        new string LinkText { get; set; }

        [UIHint(SiteUiHints.LineBreakString)] new string TeaserText { get; set; }
    }
}