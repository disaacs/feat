﻿namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHidableFromAlphanumericList
    {
        bool HideInAlphanumericListing { get; set; }
    }
}