﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using EPiServer.Core;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Epi.Sorting;

namespace Furb.Main.Lib.Epi
{
    public class PageNameGrouper : IStringGrouper
    {
        public OrderedDictionary Group(string letters, IEnumerable<PageData> pages)
        {
            var alphaDataDictionary = letters.ToDictionary(letter => letter, letter => new PageDataCollection());

            foreach (var page in pages)
            {
                var firstLetter = page.PageName.ToLower()[0];

                if (alphaDataDictionary.ContainsKey(firstLetter))
                {
                    alphaDataDictionary[page.PageName.ToLower()[0]].Add(page);
                }
            }

            var orderedDictionary = new OrderedDictionary(alphaDataDictionary.Count);
            foreach (var dic in alphaDataDictionary)
            {
                orderedDictionary.Add(dic.Key, dic.Value);
                dic.Value.Sort(new AlphabeticalPageComparer());
            }

            return orderedDictionary;
        }
    }
}