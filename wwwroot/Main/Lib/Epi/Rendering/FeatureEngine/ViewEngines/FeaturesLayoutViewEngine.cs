﻿using System.Web.Mvc;

namespace Furb.Main.Lib.Epi.Rendering.FeatureEngine.ViewEngines
{
    public class FeaturesLayoutViewEngine : RazorViewEngine
    {
        private const string FeaturePlaceholder = "%FEATURE%";

        public FeaturesLayoutViewEngine()
        {
            var viewEnginePaths = new[] {

                "~/Features/" + FeaturePlaceholder + "/{0}.cshtml",
                "~/Features/Pages/" + FeaturePlaceholder + "/{0}.cshtml",
                "~/Features/Blocks/" + FeaturePlaceholder + "/{0}.cshtml",
                "~/Features/LocalBlocks/" + FeaturePlaceholder + "/{0}.cshtml",
                "~/Features/Media/" + FeaturePlaceholder + "/{0}.cshtml",
                "~/Features/Pages/DigitalAudit/" + FeaturePlaceholder + "/{0}.cshtml",
                "~/Features/Pages/DigitalAudit/Views/{0}.cshtml",
                "~/Features/Pages/DigitalAudit/Views/partials/{0}.cshtml",
                "~/Features/Components/" + FeaturePlaceholder + "/{0}.cshtml",
                "~/Features/Shared/{0}.cshtml"
            };

            PartialViewLocationFormats = viewEnginePaths;
            ViewLocationFormats = viewEnginePaths;
            MasterLocationFormats = viewEnginePaths;
        }
    }
}
