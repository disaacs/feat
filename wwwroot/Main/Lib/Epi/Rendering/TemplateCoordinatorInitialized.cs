﻿using System;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.Framework.Web;
using EPiServer.ServiceLocation;
using EPiServer.Web;

namespace Furb.Main.Lib.Epi.Rendering
{
    public static class TemplateCoordinatorInitialized
    {
        public static void OnTemplateResolving(object sender, TemplateResolverEventArgs args)
        {
            try
            {
                if (args.ContentType == null || args.ContentType.ID == 1)
                {
                    return;
                } 

                var repo = ServiceLocator.Current.GetInstance<ITemplateRepository>();

                var listfortype = repo.List(args.ContentType.ModelType);
                var speciallayouttemplate = listfortype.FirstOrDefault(x => x.Name == "PartialTagged");

                if (args.RequestedCategory == TemplateTypeCategories.MvcPartial == args.SupportedTemplates.Any(x => x.Name == "PartialTagged") && speciallayouttemplate != null)
                {
                    args.SelectedTemplate = speciallayouttemplate;
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}