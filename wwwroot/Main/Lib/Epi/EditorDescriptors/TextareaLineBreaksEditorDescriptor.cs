﻿using System;
using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Furb.Main.Lib.Epi.EditorDescriptors
{
    [EditorDescriptorRegistration(TargetType = typeof(string), UIHint = SiteUiHints.LineBreakString)]
    public class TextareaLineBreaksEditorDescriptor : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            ClientEditingClass = "dijit/form/Textarea";

            metadata.CustomEditorSettings["uiWrapperType"] = EPiServer.Shell.UiWrapperType.Floating;

            metadata.EditorConfiguration.Add("style", "width: 300px");

            base.ModifyMetadata(metadata, attributes);
        }
    }
}