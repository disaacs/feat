﻿using System.Collections.Generic;
using System.Web.Mvc;
using Furb.Main.Lib.Epi.Attributes;

namespace Furb.Main.Lib.Epi.Adapters
{
    public class LocalizedRequiredAdapter : DataAnnotationsModelValidator<LocalizedRequiredAttribute>
    {
        public LocalizedRequiredAdapter(ModelMetadata metadata, ControllerContext context, LocalizedRequiredAttribute attribute) : base(metadata, context, attribute)
        {
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            return new[] {new ModelClientValidationRequiredRule(ErrorMessage)};
        }
    }
}