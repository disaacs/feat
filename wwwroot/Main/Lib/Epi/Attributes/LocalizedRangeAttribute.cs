﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using EPiServer.Framework.Localization;

namespace Furb.Main.Lib.Epi.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class LocalizedRangeAttribute : RangeAttribute
    {
        public LocalizedRangeAttribute(int minimum, int maximum) : base(minimum, maximum)
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedRangeAttribute), typeof(RangeAttributeAdapter));
        }

        public LocalizedRangeAttribute(double minimum, double maximum) : base(minimum, maximum)
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedRangeAttribute), typeof(RangeAttributeAdapter));
        }

        public LocalizedRangeAttribute(Type type, string minimum, string maximum) : base(type, minimum, maximum)
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedRangeAttribute), typeof(RangeAttributeAdapter));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] {new ModelClientValidationRangeRule(ErrorMessage, Minimum, Maximum)};
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture, LocalizationService.Current.GetString(ErrorMessageString), name, Minimum, Maximum);
        }
    }
}