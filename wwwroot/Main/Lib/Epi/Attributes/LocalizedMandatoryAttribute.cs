﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EPiServer.Framework.Localization;
using Furb.Main.Lib.Epi.Adapters;

namespace Furb.Main.Lib.Epi.Attributes
{
    public class LocalizedMandatoryAttribute : RequiredAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            switch (value)
            {
                case null:
                    return false;
                case bool valid:
                    return valid;
                default:
                    return false;
            }
        }

        public LocalizedMandatoryAttribute()
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedRequiredAttribute), typeof(LocalizedRequiredAdapter));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] { new ModelClientValidationRule { ValidationType = "mandatory", ErrorMessage = LocalizationService.Current.GetString(ErrorMessage) } };
        }
    }
}