﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using EPiServer.Framework.Localization;
using Furb.Main.Lib.Epi.Adapters;
using CompareAttribute = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace Furb.Main.Lib.Epi.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class LocalizedCompareAttribute : CompareAttribute
    {
        public LocalizedCompareAttribute(string otherProperty) : base(otherProperty)
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedCompareAttribute), typeof(LocalizedCompareAdapter));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] {new ModelClientValidationEqualToRule(ErrorMessage, OtherProperty)};
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture, LocalizationService.Current.GetString(ErrorMessage), name, OtherPropertyDisplayName ?? OtherProperty);
        }
    }
}