﻿using System;
using System.Web.Mvc;
using EPiServer.Framework.Localization;

namespace Furb.Main.Lib.Epi.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class LocalizedDescribedByAttribute : Attribute, IMetadataAware
    {
        private readonly string resourceName;

        public LocalizedDescribedByAttribute(string resourceName)
        {
            this.resourceName = resourceName;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues.Add("DescribedBy", resourceName.StartsWith("/") ? LocalizationService.Current.GetString(resourceName) : resourceName);
        }
    }
}