﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EPiServer.Framework.Localization;
using Furb.Main.Lib.Epi.Adapters;

namespace Furb.Main.Lib.Epi.Attributes
{
    public class LocalizedRequiredAttribute : RequiredAttribute, IClientValidatable
    {
        public LocalizedRequiredAttribute()
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedRequiredAttribute), typeof(LocalizedRequiredAdapter));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] {new ModelClientValidationRequiredRule(ErrorMessage)};
        }

        public override string FormatErrorMessage(string name)
        {
            return LocalizationService.Current.GetString(ErrorMessage);
        }
    }
}