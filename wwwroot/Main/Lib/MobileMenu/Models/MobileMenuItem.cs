﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Furb.Main.Lib.MobileMenu.Models
{
    public class MobileMenuCollection
    {
        public PageDataCollection PageDataCollection { get; set; }

        public List<MobileMenuItem> MobileMenuItemList { get; set; }
    }

    public class MobileMenuItemAsPageData : PageData
    {
        public int Id { get; set; }

        public string Header { get; set; }

        public string Url { get; set; }

        public IEnumerable<int> Children { get; set; }

        public IEnumerable<int> Path { get; set; }
    }

    public class MobileMenuItem
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public IEnumerable<int> Children { get; set; }

        public IEnumerable<int> Path { get; set; }

        public bool HideInMenuWhenAuthenticated { get; set; }
    }
}