﻿namespace Furb.Main.Lib.MobileMenu.Interfaces
{
    public interface IHideInMenuWhenAuthenticated
    {
        bool HiddenWhenAuthenticated { get; set; }
    }
}