﻿namespace Furb.Main.Lib.MobileMenu
{
    public static class MobileMenuLock
    {
        public static readonly object GetMobileMenuCollectionLock = new object();
        public static readonly object GetMobileMenuItemListLock = new object();
    }
}