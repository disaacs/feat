﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Elmah;
using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.Globalization;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Furb.Main.Lib.EpiExtensions;
using Furb.Main.Lib.Extensions;
using Furb.Main.Lib.MobileMenu.Interfaces;
using Furb.Main.Lib.MobileMenu.Models;

namespace Furb.Main.Lib.MobileMenu
{
    public static class MobileMenuCache
    {
        private const string CollectionCacheName = "MobileMenuCollection";
        private const string FilteredCacheName = "MobileMenuItemList";
        private static readonly IContentLoader ContentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
        private static readonly UrlResolver UrlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();

        public static void ClearMobileMenuCache()
        {
            foreach (var cacheKey in (from DictionaryEntry cacheItem in HttpRuntime.Cache
                where cacheItem.Key.ToString().StartsWith(CollectionCacheName)
                select cacheItem.Key.ToString()).ToList())
            {
                HttpRuntime.Cache.Remove(cacheKey);
            }

            foreach (var cacheKey in (from DictionaryEntry cacheItem in HttpRuntime.Cache
                where cacheItem.Key.ToString().StartsWith(FilteredCacheName)
                select cacheItem.Key.ToString()).ToList())
            {
                HttpRuntime.Cache.Remove(cacheKey);
            }
        }

        public static Dictionary<int, MobileMenuItem> GetMobileMenuCachedList(Type filterByType, int currentPageId = 1,  string lang = null, bool useCache = true)
        {
            var currentCultureInfo = GetCurrentCultureInfo(lang);
            var startPoint = GetStartPoint(currentPageId);

            MobileMenuCollection mobileMenuCollection;
            Dictionary<int, MobileMenuItem> mobileMenuItemList;

            lock (MobileMenuLock.GetMobileMenuCollectionLock)
            {
                if (useCache)
                {
                    var cacheName = CollectionCacheName + "_" + currentCultureInfo.TwoLetterISOLanguageName + "_" + startPoint.ID + (HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : "Anonymous");
                    mobileMenuCollection = HttpRuntime.Cache.GetOrStore(cacheName, () => GetChildren(startPoint, filterByType, currentCultureInfo), 60);
                }
                else
                {
                    mobileMenuCollection = GetChildren(startPoint, filterByType, currentCultureInfo);
                }
            }

            lock (MobileMenuLock.GetMobileMenuItemListLock)
            {
                if (useCache)
                {
                    var cacheName = FilteredCacheName + "_" + currentCultureInfo.TwoLetterISOLanguageName + "_" + startPoint.ID + "_" + (HttpContext.Current.User.Identity.IsAuthenticated ? HttpContext.Current.User.Identity.Name : "Anonymous");
                    mobileMenuItemList = HttpRuntime.Cache.GetOrStore(cacheName, () => GetMobileMenuItemList(mobileMenuCollection), 60);
                }
                else
                {
                    mobileMenuItemList = GetMobileMenuItemList(mobileMenuCollection);
                }
            }

            if (mobileMenuItemList == null || mobileMenuItemList.Count == 0)
            {
                ClearMobileMenuCache();
            }

            if (mobileMenuItemList != null)
            {
                AddMissingItemInCurrentPath(filterByType, currentPageId, mobileMenuItemList, startPoint);
            }

            return mobileMenuItemList;
        }

        public static PageReference GetStartPoint(int currentId)
        {
            var currentPage = ContentLoader.Get<PageData>(new ContentReference(currentId));

            var startPoint = ContentReference.RootPage;

            if (currentPage is IHasOwnMenu)
            {
                startPoint = currentPage.PageLink;
            }
            else
            {
                var ancestors = currentPage.GetAncestors();

                foreach (var pageData in ancestors)
                {
                    if (pageData is IHasOwnMenu)
                    {
                        startPoint = pageData.PageLink;
                    }
                }
            }

            return startPoint;
        }
        
        private static IEnumerable<PageData> GetChildren(PageData pageData, Type filterByType, CultureInfo lang)
        {
            foreach (var child in pageData.GetChildren(lang).Where(filterByType.IsInstanceOfType).Where(item => item.VisibleInMenu || item is IAlwaysVisibleInMobileMenu))
            {
                foreach (var subChild in GetChildren(child, filterByType, lang))
                {
                    if (!(child is IHideChildrenInMenu))
                    {
                        yield return subChild;
                    }
                }
                yield return child;
            }
        }

        private static IEnumerable<int> GetChildrenIdList(IEnumerable<PageData> children, Type filterByType)
        {
            return children.Where(filterByType.IsInstanceOfType)
                .Where(x => (x.VisibleInMenu) && x.CheckPublishedStatus(PagePublishedStatus.Published))
                .Select(pageData => pageData.PageLink.ID).ToList();
        }

        private static CultureInfo GetCurrentCultureInfo(string lang)
        {
            if (lang == null)
            {
                lang = ContentLanguage.PreferredCulture.TwoLetterISOLanguageName;
            }
            else
            {
                ContentLanguage.PreferredCulture = new CultureInfo(lang);
            }

            var currentCultureInfo = new CultureInfo(lang);
            return currentCultureInfo;
        }

        private static Dictionary<int, MobileMenuItem> GetMobileMenuItemList(MobileMenuCollection mobileMenuCollection)
        {
            try
            {
                var isAuthenticated = PrincipalInfo.CurrentPrincipal.Identity.IsAuthenticated;

                var pageDataCollection = new PageDataCollection();

                foreach (var pageData in mobileMenuCollection.PageDataCollection)
                {
                    if (isAuthenticated && pageData is IHideInMenuWhenAuthenticated && ((IHideInMenuWhenAuthenticated)pageData).HiddenWhenAuthenticated)
                    {
                        continue;
                    }

                    pageDataCollection.Add(pageData);
                }

                var filteredList = FilterForVisitor.Filter(pageDataCollection);

                filteredList.Add(ContentReference.RootPage.GetPage());

                var hashSet = new HashSet<int>(filteredList.Select(item => item.PageLink.ID));

                var mobilemenuItemList = isAuthenticated ? mobileMenuCollection.MobileMenuItemList.Where(x => !x.HideInMenuWhenAuthenticated) : mobileMenuCollection.MobileMenuItemList;
                var result = mobilemenuItemList.Where(item => hashSet.Contains(item.Id)).ToDictionary(item => item.Id);
                foreach (var mobileMenuItem in result.Values)
                {
                    mobileMenuItem.Children = mobileMenuItem.Children.Where(hashSet.Contains);
                }

                return result;
            }
            catch (Exception exception)
            {
                ErrorSignal.FromCurrentContext().Raise(exception, HttpContext.Current);
                return new Dictionary<int, MobileMenuItem>();
            }
        }

        private static void AddMissingItemInCurrentPath(Type filterByType, int currentPageId, IDictionary<int, MobileMenuItem> mobileMenuItemList, ContentReference startPoint)
        {
            var currentPage = ContentLoader.Get<PageData>(new ContentReference(currentPageId));

            var currentPath = new List<PageData> { currentPage };
            currentPath.AddRange(currentPage.GetAncestors().ToList());

            foreach (var pageData in currentPath)
            {
                if (mobileMenuItemList.ContainsKey(pageData.PageLink.ID))
                {
                    continue;
                }

                var path = startPoint == ContentReference.RootPage ? GetPathForRootPage(pageData) : GetPathForStartPoint(startPoint, pageData);

                mobileMenuItemList.Add(pageData.PageLink.ID, new MobileMenuItem
                {
                    Children = GetChildrenIdList(pageData.GetChildren(), filterByType).ToList(),
                    Id = pageData.PageLink.ID,
                    Name = pageData.Name,
                    Path = path
                });
            }
        }

        private static MobileMenuCollection GetChildren(ContentReference startPoint, Type filterByType, CultureInfo lang)
        {
            var mobileMenuCollection = new MobileMenuCollection();

            var startPointPage = ContentLoader.Get<PageData>(startPoint);
            var rootPage = ContentLoader.Get<PageData>(ContentReference.RootPage);
            var decedents = GetChildren(startPointPage, filterByType, lang);

            mobileMenuCollection.PageDataCollection =
                new PageDataCollection { startPointPage, decedents.ToPageDataCollection() };

            if (startPoint != ContentReference.RootPage)
            {
                mobileMenuCollection.PageDataCollection.Add(rootPage);
                mobileMenuCollection.PageDataCollection.Add(ContentReference.StartPage.GetPage());
            }

            mobileMenuCollection.MobileMenuItemList = new List<MobileMenuItem>();

            foreach (var pageData in mobileMenuCollection.PageDataCollection)
            {
                var children = GetChildrenIdList(pageData.GetChildren(), filterByType).ToList();
                var path = startPoint == ContentReference.RootPage ? GetPathForRootPage(pageData) : GetPathForStartPoint(startPoint, pageData);

                if (startPoint != ContentReference.RootPage)
                {
                    if (pageData.PageLink == ContentReference.StartPage)
                    {
                        path = new List<int> { ContentReference.StartPage.ID, rootPage.PageLink.ID };
                        children = new List<int>();
                    }

                    if (pageData.PageLink == startPoint)
                    {
                        path = new List<int> { startPoint.ID, rootPage.PageLink.ID };
                    }

                    if (pageData.PageLink == rootPage.PageLink)
                    {
                        children = new List<int> { startPoint.ID, ContentReference.StartPage.ID };
                    }
                }

                mobileMenuCollection.MobileMenuItemList.Add(new MobileMenuItem
                {
                    Id = pageData.PageLink.ID,
                    Path = path,
                    Name = pageData.PageName,
                    Url = GetUrl(pageData),
                    Children = children,
                    HideInMenuWhenAuthenticated = pageData is IHideInMenuWhenAuthenticated && ((IHideInMenuWhenAuthenticated)pageData).HiddenWhenAuthenticated
                });
            }

            return mobileMenuCollection;
        }

        private static List<int> GetPathForRootPage(PageData pageData)
        {
            var ancestors = ContentLoader.GetAncestors(pageData.PageLink);
            var ancestorIds = ancestors.Select(ancestor => ancestor.ContentLink.ID).ToList();

            ancestorIds.Insert(0, pageData.PageLink.ID);

            return ancestorIds;
        }

        private static List<int> GetPathForStartPoint(ContentReference startPoint, PageData pageData)
        {
            var ancestorIds = new List<int>();

            var ancestors = ContentLoader.GetAncestors(pageData.PageLink);

            foreach (var ancestor in ancestors)
            {
                ancestorIds.Add(ancestor.ContentLink.ID);

                if (ancestor.ContentLink.ID == startPoint.ID)
                {
                    break;
                }
            }

            ancestorIds.Add(ContentReference.RootPage.ID);

            ancestorIds.Insert(0, pageData.PageLink.ID);

            return ancestorIds.ToList();
        }

        private static string GetUrl(IContent pageData)
        {
            var url = UrlResolver.GetUrl(pageData.ContentLink);

            return !string.IsNullOrWhiteSpace(url) ? url.ToLowerInvariant() : string.Empty;
        }
    }
}