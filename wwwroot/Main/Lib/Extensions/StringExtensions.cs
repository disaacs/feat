﻿using System;
using System.Text.RegularExpressions;
using System.Web;

namespace Furb.Main.Lib.Extensions
{
    public static class StringExtensions
    {
        public static string StripHtml(this string inputString)
        {
            return Regex.Replace(inputString, "<.*?>", string.Empty);
        }

        public static string GetBaseUrl()
        {
            var port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            port = string.IsNullOrEmpty(port) || port == "80" || port == "443" ? "" : ":" + port;

            var protocol = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            protocol = protocol == "0" ? "http://" : "https://";

            var url = $"{protocol}{HttpContext.Current.Request.ServerVariables["SERVER_NAME"]}{port}";

            return url.EndsWith("/") ? url.Remove(url.LastIndexOf("/", StringComparison.Ordinal)) : url;
        }

        public static string ToIntroString(this string inText, int length)
        {
            if (inText.Length > length)
            {
                var utText = "";

                for (var i = 0; i < length; i++)
                {
                    if (i > length - 50)
                    {
                        if (inText[i] == ',' || inText[i] == '.' || inText[i] == '!' || inText[i] == '?')
                        {
                            utText = utText + inText[i];
                            return utText;
                        }
                    }

                    if (i > length - 15)
                    {
                        if (inText[i] == ' ')
                        {
                            utText = utText + inText[i];

                            return utText;
                        }
                    }

                    utText = utText + inText[i];
                }

                return utText;
            }

            return inText;
        }

        public static string AddQueryParam(this string source, string key, string value)
        {
            string delim;

            if ((source == null) || !source.Contains("?"))
            {
                delim = "?";
            }
            else if (source.EndsWith("?") || source.EndsWith("&"))
            {
                delim = string.Empty;
            }
            else
            {
                delim = "&";
            }

            return source + delim + HttpUtility.UrlEncode(key) + "=" + HttpUtility.UrlEncode(value);
        }
    }
}