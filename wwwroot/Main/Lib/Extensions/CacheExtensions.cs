﻿using System;
using System.Web.Caching;

namespace Furb.Main.Lib.Extensions
{
    public static class CacheExtensions
    {
        private static readonly object Sync = new object();

        public static T GetOrStore<T>(this Cache cache, string key, Func<T> generator, double expireInMinutes)
        {
            return cache.GetOrStore(key, (cache[key] == null && generator != null) ? generator() : default(T), expireInMinutes);
        }

        private static T GetOrStore<T>(this Cache cache, string key, T obj, double expireInMinutes)
        {
            var result = cache[key];

            if (result != null)
            {
                return (T)result;
            }

            lock (Sync)
            {
                result = cache[key];

                if (result != null)
                {
                    return (T)result;
                }

                // ReSharper disable CompareNonConstrainedGenericWithNull
                // ReSharper disable ConvertConditionalTernaryToNullCoalescing
                result = obj != null ? obj : default(T);
                // ReSharper restore ConvertConditionalTernaryToNullCoalescing
                // ReSharper restore CompareNonConstrainedGenericWithNull
                if(result != null) {
                    cache.Insert(key, result, null, DateTime.Now.AddMinutes(expireInMinutes), Cache.NoSlidingExpiration);
                }
            }

            return (T)result;
        }
    }
}