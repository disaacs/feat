﻿namespace Furb.Main.Lib.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsNull(this object value)
        {
            return value == null;
        }

        public static bool IsNotNull(this object value)
        {
            return !value.IsNull();
        }

        public static bool IsNot<T>(this object instance)
        {
            return !(instance is T);
        }
    }
}