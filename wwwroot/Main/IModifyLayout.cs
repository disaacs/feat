﻿namespace Furb.Main
{
    internal interface IModifyLayout
    {
        void ModifyLayout(LayoutModel layoutModel);
    }
}