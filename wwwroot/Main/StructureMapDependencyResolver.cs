﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StructureMap;

namespace Furb.Main
{
    public class StructureMapDependencyResolver : IDependencyResolver
    {
        private readonly IContainer container;

        public StructureMapDependencyResolver(IContainer container) => this.container = container;

        public object GetService(Type serviceType)
        {
            if (serviceType.IsInterface || serviceType.IsAbstract)
            {
                return GetInterfaceService(serviceType);
            }

            return GetConcreteService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType) => container.GetAllInstances(serviceType).Cast<object>();

        private object GetConcreteService(Type serviceType) => container.GetInstance(serviceType);

        private object GetInterfaceService(Type serviceType) => container.TryGetInstance(serviceType);
    }
}